# README #

### What is this repository for? ###

* This project is an Android app that reads sensors in real-time from the Microsoft Band and sends 
the sensor data to Azure's IoT Hub.
* This project was developed at the [Computational Health Informatics Lab](https://chil.uwaterloo.ca/) in the 
[University of Waterloo](https://cs.uwaterloo.ca/)
* As of January 2017, this project is no longer being supported.
* If you have any question related to this project, please contact me by opening an issue.
* See LICENSE.txt for legal info.

### High Level Notes ###
* The app is capable of reading the following sensors:
    * Accelerometer
    * Ambient Light
    * Barometer
    * Connection state
    * Contact state
    * Distance
    * GSR
    * Gyroscope
    * Heart Rate
    * RR Interval
    * Skin temperature
    * UV
* Sensor data is stored in the device's database in real-time
* The app uses Android's SyncAdapter and SyncService to send data to the back end:
    * Sensor data is sent to Azure's IoT Hub
    * App data is sent to a REST API back-end




_Licensed under the Non-Profit Open Software License version 3.0_

_Copyright (c) 2016-2017. Pablo A. Carbajal Siller._  
_All rights reserved. No warranty, explicit or implicit, provided._
