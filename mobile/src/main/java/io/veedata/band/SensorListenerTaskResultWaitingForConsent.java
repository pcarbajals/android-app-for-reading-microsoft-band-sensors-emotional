/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.band;

import io.veedata.band.sensor.SensorListenerTaskResult;

/**
 * Created by great_000 on 9/20/2016.
 */
public class SensorListenerTaskResultWaitingForConsent extends SensorListenerTaskResult {
    public SensorListenerTaskResultWaitingForConsent(final String message) {
        super(message);
    }

    @Override
    public boolean isConnected() {
        return true;
    }
}
