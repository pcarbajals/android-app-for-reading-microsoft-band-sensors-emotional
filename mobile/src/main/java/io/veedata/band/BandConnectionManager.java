/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.band;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.microsoft.band.BandClient;
import com.microsoft.band.BandClientManager;
import com.microsoft.band.BandException;
import com.microsoft.band.BandInfo;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.UserConsent;
import com.microsoft.band.sensors.HeartRateConsentListener;

import java.lang.ref.WeakReference;

import io.veedata.band.sensor.SensorListenerException;
import io.veedata.band.sensor.SensorListenerManager;
import io.veedata.band.sensor.SensorListenerService;
import io.veedata.band.sensor.SensorListenerTaskResult;
import io.veedata.band.sensor.SensorListenerTaskResultConnected;
import io.veedata.band.sensor.SensorListenerTaskResultError;
import io.veedata.veemotion.MainActivity;

/**
 * Created by great_000 on 4/29/2016.
 */
public class BandConnectionManager {

    private static final String LOG_TAG = BandConnectionManager.class.getSimpleName();

    private static BandConnectionManager mInstance;

    private BandClient mBandClient = null;

    public BandConnectionManager() {
    }

    public static synchronized BandConnectionManager getInstance() {
        if (mInstance == null) {
            mInstance = new BandConnectionManager();
        }

        return mInstance;
    }

    public static boolean isConnectedToBand() {
        return getInstance().mBandClient != null && getInstance().mBandClient.isConnected();
    }

    public static boolean hasPairedBands() {
        if (BandClientManager.getInstance().getPairedBands().length > 0) {
            return true;
        }

        return false;
    }

    private void connectToBandClient(final MainActivity activity) throws SensorListenerException {
        Log.i(LOG_TAG, "Checking the connection to band client.");

        /*
         * Here is the process for connecting to the Band Client
         * 1. Connect to the Band client
         * 2. Ask for user's consent to read heart rate
         */
        if (mBandClient == null) {
            Log.d(LOG_TAG, "We have no reference to the band client.");
            BandInfo[] devices = BandClientManager.getInstance().getPairedBands();
            if (devices.length == 0) {
                throw new SensorListenerException("Band isn't paired with your phone.\n");
            }

            Log.d(LOG_TAG, "Creating a reference to the band client.");
            mBandClient = BandClientManager.getInstance().create(activity, devices[0]);
        }

        if (mBandClient.getConnectionState() != ConnectionState.CONNECTED) {
            ConnectionState connectionState;
            String connectionFailedMessage = "Band isn't connected. Please make sure bluetooth is on and the band is in range.\n";

            try {
                Log.i(LOG_TAG, "Connecting to Band...\n");
                connectionState = mBandClient.connect().await();

                if (connectionState == ConnectionState.CONNECTED) {
                    Log.e(LOG_TAG, "Unable to connect to the Band.");
                } else {
                    throw new SensorListenerException(connectionFailedMessage);
                }

            } catch (InterruptedException e) {
                throw new SensorListenerException(connectionFailedMessage, e);

            } catch (BandException e) {
                String exceptionMessage = "";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occurred: " + e.getMessage() + "\n";
                }
                throw new SensorListenerException(exceptionMessage, e);
            }
        }
    }

    @NonNull
    public SensorListenerTaskResult startRecording(final WeakReference<Activity> param) {
        Log.i(LOG_TAG, "BandConnectionManager is starting recording");
        final MainActivity activity = (MainActivity) param.get();

        try {
            connectToBandClient(activity);

        } catch (SensorListenerException e) {
            final String message = "Unable to connect to the Band.";

            Log.e(LOG_TAG, "Unable to connect to the Band.", e);

            return new SensorListenerTaskResultError(message, e);
        }

        if (mBandClient == null) {
            return new SensorListenerTaskResultError("Band client is not available.\n");
        }

        Log.d(LOG_TAG, "Checking for HR user consent.");
        final UserConsent currentHeartRateConsent = mBandClient.getSensorManager().getCurrentHeartRateConsent();

        if (currentHeartRateConsent == UserConsent.GRANTED) {
            Log.d(LOG_TAG, "HR user consent is granted, starting recording.");
            SensorListenerManager.startRecording(activity, mBandClient);
            return new SensorListenerTaskResultConnected("Sending request to listen to Band sensors.");

        } else {
            Log.d(LOG_TAG, "HR user consent hasn't been granted, requesting user for grant.");
            mBandClient.getSensorManager().requestHeartRateConsent(activity, new HeartRateConsentListener() {
                @Override
                public void userAccepted(boolean consentGiven) {
                    if (consentGiven) {
                        Log.d(LOG_TAG, "User gave consent, starting recording.");
                        SensorListenerManager.startRecording(activity, mBandClient);

                    } else {
                        Log.w(LOG_TAG, "User did not give this application consent to access Band heart rate data.");
                    }
                }
            });

            return new SensorListenerTaskResultWaitingForConsent("Requesting user consent.");
        }
    }
}
