/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.band.sensor;

/**
 * Created by Pablo on 3/2/2016.
 */
public class SensorListenerTaskResultError extends io.veedata.band.sensor.SensorListenerTaskResult {
    private Throwable mCause;

    public SensorListenerTaskResultError(final String message) {
        super(message);
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    public SensorListenerTaskResultError(final String message, final Throwable cause) {
        super(message);
        mCause = cause;
    }
}
