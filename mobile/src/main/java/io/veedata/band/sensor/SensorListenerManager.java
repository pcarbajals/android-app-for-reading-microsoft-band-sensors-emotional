/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.band.sensor;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.microsoft.band.BandClient;
import com.microsoft.band.BandConnectionCallback;
import com.microsoft.band.BandException;
import com.microsoft.band.BandIOException;
import com.microsoft.band.InvalidBandVersionException;
import com.microsoft.band.sensors.BandAccelerometerEventListener;
import com.microsoft.band.sensors.BandAmbientLightEventListener;
import com.microsoft.band.sensors.BandBarometerEventListener;
import com.microsoft.band.sensors.BandContactEventListener;
import com.microsoft.band.sensors.BandDistanceEventListener;
import com.microsoft.band.sensors.BandGsrEventListener;
import com.microsoft.band.sensors.BandGyroscopeEventListener;
import com.microsoft.band.sensors.BandHeartRateEventListener;
import com.microsoft.band.sensors.BandRRIntervalEventListener;
import com.microsoft.band.sensors.BandSkinTemperatureEventListener;
import com.microsoft.band.sensors.BandUVEventListener;
import com.microsoft.band.sensors.SampleRate;

/**
 * Created by Pablo on 2/29/2016.
 */
public class SensorListenerManager {
    private static final String LOG_TAG = "SensorListenerManager";

    private static SensorListenerManager mInstance;
    private static boolean isListeningHeartRateEvents = false;
    private BandClient mBandClient;

    private SensorListenerManager() {
    }

    public static synchronized SensorListenerManager getInstance() {
        if (mInstance == null) {
            mInstance = new SensorListenerManager();

        }

        return mInstance;
    }

    public static boolean registerAccelerometerEventListener(final BandAccelerometerEventListener listener, final SampleRate sampleRate) throws BandIOException {
        return getInstance().mBandClient.getSensorManager().registerAccelerometerEventListener(listener, sampleRate);
    }

    public static void unregisterAccelerometerEventListener(final BandAccelerometerEventListener listener) throws BandIOException {
        getInstance().mBandClient.getSensorManager().unregisterAccelerometerEventListener(listener);
    }

    public static boolean registerAmbientLightEventListener(final BandAmbientLightEventListener listener) throws BandIOException, InvalidBandVersionException {
        return getInstance().mBandClient.getSensorManager().registerAmbientLightEventListener(listener);
    }

    public static void unregisterAmbientLightEventListener(final BandAmbientLightEventListener listener) throws BandIOException {
        getInstance().mBandClient.getSensorManager().unregisterAmbientLightEventListener(listener);
    }

    public static boolean registerBarometerEventListener(final BandBarometerEventListener listener) throws BandIOException, InvalidBandVersionException {
        return getInstance().mBandClient.getSensorManager().registerBarometerEventListener(listener);
    }

    public static void unregisterBarometerEventListener(final BandBarometerEventListener listener) throws BandIOException {
        getInstance().mBandClient.getSensorManager().unregisterBarometerEventListener(listener);
    }

    public static void registerConnectionCallbackListener(final BandConnectionCallback bandConnectionCallback) {
        getInstance().mBandClient.registerConnectionCallback(bandConnectionCallback);
    }

    public static boolean registerContactEventListener(final BandContactEventListener contactEventListener) throws BandIOException {
        return getInstance().mBandClient.getSensorManager().registerContactEventListener(contactEventListener);
    }

    public static boolean registerDistanceEventListener(final BandDistanceEventListener listener) throws BandIOException, InvalidBandVersionException {
        return getInstance().mBandClient.getSensorManager().registerDistanceEventListener(listener);
    }

    public static void unregisterDistanceEventListener(final BandDistanceEventListener listener) throws BandIOException {
        getInstance().mBandClient.getSensorManager().unregisterDistanceEventListener(listener);
    }

    public static boolean registerSkinTemperatureEventListener(final BandSkinTemperatureEventListener skinTempListener) throws BandIOException {
        return getInstance().mBandClient.getSensorManager().registerSkinTemperatureEventListener(skinTempListener);
    }

    public static boolean registerGsrEventListener(final BandGsrEventListener gsrEventListener) throws BandIOException, InvalidBandVersionException {
        return getInstance().mBandClient.getSensorManager().registerGsrEventListener(gsrEventListener);
    }

    public static boolean registerGyroscopeEventListener(final BandGyroscopeEventListener listener, final SampleRate sampleRate) throws BandIOException {
        return getInstance().mBandClient.getSensorManager().registerGyroscopeEventListener(listener, sampleRate);
    }

    public static void unregisterGyroscopeEventListener(final BandGyroscopeEventListener listener) throws BandIOException {
        getInstance().mBandClient.getSensorManager().unregisterGyroscopeEventListener(listener);
    }

    static void registerHeartRateEventsListener(final SensorEventListener eventListener) throws BandException, InvalidBandVersionException {
        isListeningHeartRateEvents = true;
        getInstance().mBandClient.getSensorManager().registerHeartRateEventListener(eventListener);
        getInstance().mBandClient.getSensorManager().registerRRIntervalEventListener(eventListener);
    }

    static void unregisterHeartRateEventListener(final SensorEventListener eventListener) throws BandException {
        isListeningHeartRateEvents = false;
        getInstance().mBandClient.getSensorManager().unregisterHeartRateEventListener(eventListener);
        getInstance().mBandClient.getSensorManager().unregisterRRIntervalEventListener(eventListener);
    }

    public static boolean registerUVEventListener(final BandUVEventListener listener) throws BandIOException, InvalidBandVersionException {
        return getInstance().mBandClient.getSensorManager().registerUVEventListener(listener);
    }

    public static void unregisterUVEventListener(final BandUVEventListener listener) throws BandIOException {
        getInstance().mBandClient.getSensorManager().unregisterUVEventListener(listener);
    }

    static void unregisterSkinTemperatureEventListener(final BandSkinTemperatureEventListener skinTempListener) throws BandIOException {
        getInstance().mBandClient.getSensorManager().unregisterSkinTemperatureEventListener(skinTempListener);
    }

    static void unregisterGsrEventListener(final BandGsrEventListener gsrEventListener) throws BandIOException {
        getInstance().mBandClient.getSensorManager().unregisterGsrEventListener(gsrEventListener);
    }

    public static void startRecording(Activity activity, final BandClient bandClient) {
        getInstance().mBandClient = bandClient;

        Log.i(LOG_TAG, "Sending start listening message to SensorListenerService");
        Intent intent = new Intent(activity, SensorListenerService.class);
        intent.setAction(SensorListenerService.ACTION_START_LISTENING);

        activity.startService(intent);
    }

    public static boolean isListeningHeartRateEvents() {
        return isListeningHeartRateEvents;
    }

}
