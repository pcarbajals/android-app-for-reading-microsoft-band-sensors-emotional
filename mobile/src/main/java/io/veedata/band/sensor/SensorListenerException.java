/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.band.sensor;

/**
 * Created by Pablo on 3/1/2016.
 */
public class SensorListenerException extends Throwable {
    public SensorListenerException(String detailedMessage) {
        super(detailedMessage);
    }

    public SensorListenerException(String detailedMessage, Throwable cause) {
        super(detailedMessage, cause);
    }
}
