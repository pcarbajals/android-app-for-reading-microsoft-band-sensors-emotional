/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.band.sensor;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Process;
import android.support.annotation.Nullable;
import android.util.Log;

import com.microsoft.band.BandConnectionCallback;
import com.microsoft.band.BandException;
import com.microsoft.band.BandIOException;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.InvalidBandVersionException;
import com.microsoft.band.sensors.BandContactEvent;
import com.microsoft.band.sensors.BandContactEventListener;
import com.microsoft.band.sensors.BandContactState;
import com.microsoft.band.sensors.SampleRate;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import io.veedata.veemotion.database.sensor.ConnectionStateEventContract;
import io.veedata.veemotion.database.sensor.ContactStateEventContract;

/**
 * Do not invoke this class directly, use instead SensorListenerManager to start this service.
 * SensorListenerManager performs some preparation work that this service requires.
 */
public class SensorListenerService extends IntentService {
    private static final String LOG_TAG = "SensorListenerService";

    public static final String ACTION_START_LISTENING = "io.veedata.band.data.action.START_LISTENING";

    public static final int TEN_SECONDS = 10000;
    public static final int ONE_MINUTE = 60000;
    public static final int FIVE_MINUTES = 300000;
    public static final int SIX_MINUTES = 360000;
    public static int PAUSE_MONITOR_MILLIS = 2000;

    private BandStatusListener mBandStatusListener = new BandStatusListener();
    private SensorEventListener mSensorEventListener = new SensorEventListener(this);
    private Intent mIntent;

    /**
     * Field for acquiring and releasing the power lock that allows the CPU to remain awake so that
     * the app can continue reading and writing sensor data.
     */
    private PowerManager.WakeLock mWakeLock;

    private Handler mHrSensorsHandler = new Handler();

    private final Runnable mHrSensorsMonitorRunnable = new Runnable() {
        @Override
        public void run() {
            Log.i(LOG_TAG, "Sensor monitor is running");
            if (mBandStatusListener.isWorn()) {
                if (SensorListenerManager.isListeningHeartRateEvents()) {
                    try {
                        SensorListenerManager.unregisterHeartRateEventListener(mSensorEventListener);

                        Log.i(LOG_TAG, "Stop listening heart rate for " + FIVE_MINUTES + " s.");
                        mHrSensorsHandler.postDelayed(mHrSensorsMonitorRunnable, 1);

                    } catch (BandException e) {
                        Log.e(LOG_TAG, "Error unregistering HR listener", e);
                    }
                } else {
                    try {
                        SensorListenerManager.registerHeartRateEventsListener(mSensorEventListener);

                        Log.i(LOG_TAG, "Start listening heart rate for " + SIX_MINUTES + " s.");
                        // pause for a period, then checking away will unregister the HR listener
                        mHrSensorsHandler.postDelayed(mHrSensorsMonitorRunnable, SIX_MINUTES);

                    } catch (BandException | InvalidBandVersionException e) {
                        Log.e(LOG_TAG, "Error registering HR listener", e);
                    }
                }
            } else {
                // wait until the user wears the band again
                mHrSensorsHandler.postDelayed(mHrSensorsMonitorRunnable, FIVE_MINUTES);
            }
        }
    };


    public SensorListenerService() {
        super("SensorListenerService");
    }

    @Override
    public boolean stopService(final Intent name) {
        Log.i(LOG_TAG, "stopService");
        return super.stopService(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mIntent = intent;
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_START_LISTENING.equals(action)) {
                try {
                    registerStatusListeners();
                    registerSensorListeners();

                } catch (BandException | InvalidBandVersionException e) {
                    Log.e(LOG_TAG, "Error registering listeners onHandleIntent()", e);
                }

                mHrSensorsHandler.removeCallbacks(mHrSensorsMonitorRunnable);
                mHrSensorsHandler.post(mHrSensorsMonitorRunnable);
            }
        }
    }

    private void registerStatusListeners() throws BandIOException {
        SensorListenerManager.registerContactEventListener(mBandStatusListener);
        SensorListenerManager.registerConnectionCallbackListener(mBandStatusListener);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                LOG_TAG + "Lock");
    }

    @Nullable
    @Override
    public IBinder onBind(final Intent intent) {
        mWakeLock.acquire();
        return super.onBind(intent);
    }

    @Override
    public boolean onUnbind(final Intent intent) {
        mWakeLock.release();
        return super.onUnbind(intent);
    }

    private void registerSensorListeners() throws BandException, InvalidBandVersionException {
//        SensorListenerManager.registerAccelerometerEventListener(mSensorEventListener, SampleRate.MS128);
//        SensorListenerManager.registerAmbientLightEventListener(mSensorEventListener);
//        SensorListenerManager.registerBarometerEventListener(mSensorEventListener);
//        SensorListenerManager.registerDistanceEventListener(mSensorEventListener);
        SensorListenerManager.registerGsrEventListener(mSensorEventListener);
//        SensorListenerManager.registerGyroscopeEventListener(mSensorEventListener, SampleRate.MS128);
        SensorListenerManager.registerHeartRateEventsListener(mSensorEventListener);
        SensorListenerManager.registerSkinTemperatureEventListener(mSensorEventListener);
//        SensorListenerManager.registerUVEventListener(mSensorEventListener);
    }

    private void unregisterEventListener() throws BandException {
//        SensorListenerManager.unregisterAccelerometerEventListener(mSensorEventListener);
//        SensorListenerManager.unregisterAmbientLightEventListener(mSensorEventListener);
//        SensorListenerManager.unregisterBarometerEventListener(mSensorEventListener);
//        SensorListenerManager.unregisterDistanceEventListener(mSensorEventListener);
        SensorListenerManager.unregisterGsrEventListener(mSensorEventListener);
//        SensorListenerManager.unregisterGyroscopeEventListener(mSensorEventListener);
        SensorListenerManager.unregisterHeartRateEventListener(mSensorEventListener);
        SensorListenerManager.unregisterSkinTemperatureEventListener(mSensorEventListener);
//        SensorListenerManager.unregisterUVEventListener(mSensorEventListener);
    }

    private void logWarning(final String msg) {
        Log.w(LOG_TAG, msg);
    }

    private class BandStatusListener implements BandContactEventListener, BandConnectionCallback {
        // Always assume the band is initially worn
        private boolean mIsWorn = true;

        @Override
        public void onBandContactChanged(final BandContactEvent event) {
            if (event == null) {
                logWarning("Received a null event!");
                return;
            }

            String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

            final BandContactState contactState = event.getContactState();
            Log.d(LOG_TAG, "BandConnectState: " + contactState);

            ContentValues values = new ContentValues();
            values.put(ContactStateEventContract.COLUMN_TIME_STAMP, timeStamp);
            values.put(ContactStateEventContract.COLUMN_NAME_STATE, contactState.toString());
            getContentResolver().insert(ContactStateEventContract.CONTENT_URI, values);

            try {
                if (contactState == BandContactState.NOT_WORN) {
                    Log.i(LOG_TAG, "Band is not worn, stop listening.");
                    mIsWorn = false;
                    unregisterEventListener();

                } else if (contactState == BandContactState.WORN) {
                    Log.i(LOG_TAG, "Band is worn, resume listening.");
                    mIsWorn = true;
                    registerSensorListeners();
                }
            } catch (BandException | InvalidBandVersionException e) {
                Log.e(LOG_TAG, "Error handling onBandContactChanged()", e);
            }

        }

        @Override
        public void onStateChanged(final ConnectionState connectionState) {
            Log.d(LOG_TAG, "Band connection state changed to: " + connectionState);

            String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

            ContentValues values = new ContentValues();
            values.put(ConnectionStateEventContract.COLUMN_TIME_STAMP, timeStamp);
            values.put(ConnectionStateEventContract.COLUMN_NAME_STATE, connectionState.toString());
            getContentResolver().insert(ConnectionStateEventContract.CONTENT_URI, values);
        }

        public boolean isWorn() {
            return mIsWorn;
        }
    }

}
