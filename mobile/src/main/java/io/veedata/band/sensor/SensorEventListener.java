/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.band.sensor;

import android.content.ContentValues;
import android.net.Uri;
import android.util.Log;

import com.microsoft.band.InvalidBandVersionException;
import com.microsoft.band.sensors.BandAccelerometerEvent;
import com.microsoft.band.sensors.BandAccelerometerEventListener;
import com.microsoft.band.sensors.BandAmbientLightEvent;
import com.microsoft.band.sensors.BandAmbientLightEventListener;
import com.microsoft.band.sensors.BandBarometerEvent;
import com.microsoft.band.sensors.BandBarometerEventListener;
import com.microsoft.band.sensors.BandDistanceEvent;
import com.microsoft.band.sensors.BandDistanceEventListener;
import com.microsoft.band.sensors.BandGsrEvent;
import com.microsoft.band.sensors.BandGsrEventListener;
import com.microsoft.band.sensors.BandGyroscopeEvent;
import com.microsoft.band.sensors.BandGyroscopeEventListener;
import com.microsoft.band.sensors.BandHeartRateEvent;
import com.microsoft.band.sensors.BandHeartRateEventListener;
import com.microsoft.band.sensors.BandRRIntervalEvent;
import com.microsoft.band.sensors.BandRRIntervalEventListener;
import com.microsoft.band.sensors.BandSkinTemperatureEvent;
import com.microsoft.band.sensors.BandSkinTemperatureEventListener;
import com.microsoft.band.sensors.BandUVEvent;
import com.microsoft.band.sensors.BandUVEventListener;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import io.veedata.veemotion.database.sensor.AccelerometerEventContract;
import io.veedata.veemotion.database.sensor.AmbientLightEventContract;
import io.veedata.veemotion.database.sensor.BarometerEventContract;
import io.veedata.veemotion.database.sensor.DistanceEventContract;
import io.veedata.veemotion.database.sensor.GsrEventContract;
import io.veedata.veemotion.database.sensor.GyroscopeEventContract;
import io.veedata.veemotion.database.sensor.HeartRateEventContract;
import io.veedata.veemotion.database.sensor.RRIntervalEventContract;
import io.veedata.veemotion.database.sensor.SkinTemperatureEventContract;
import io.veedata.veemotion.database.sensor.UVEventContract;

/**
 * Created by great_000 on 4/25/2016.
 */
class SensorEventListener implements BandAccelerometerEventListener, BandAmbientLightEventListener, BandBarometerEventListener, BandDistanceEventListener, BandGyroscopeEventListener, BandHeartRateEventListener, BandRRIntervalEventListener, BandGsrEventListener, BandSkinTemperatureEventListener, BandUVEventListener {
    private static final String LOG_TAG = SensorEventListener.class.getSimpleName();

    private SensorListenerService mSensorListenerService;

    public SensorEventListener(final SensorListenerService sensorListenerService) {
        mSensorListenerService = sensorListenerService;
    }

    @Override
    public void onBandAmbientLightChanged(final BandAmbientLightEvent event) {
        String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

        ContentValues values = new ContentValues();
        values.put(AmbientLightEventContract.COLUMN_TIME_STAMP, timeStamp);
        values.put(AmbientLightEventContract.COLUMN_NAME_BRIGHTNESS, event.getBrightness());
        insert(AmbientLightEventContract.CONTENT_URI, values);
    }

    private void insert(final Uri contentUri, final ContentValues values) {
        mSensorListenerService.getContentResolver().insert(contentUri, values);
    }

    @Override
    public void onBandAccelerometerChanged(final BandAccelerometerEvent event) {
        String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

        ContentValues values = new ContentValues();
        values.put(AccelerometerEventContract.COLUMN_TIME_STAMP, timeStamp);
        values.put(AccelerometerEventContract.COLUMN_NAME_X, event.getAccelerationX());
        values.put(AccelerometerEventContract.COLUMN_NAME_Y, event.getAccelerationY());
        values.put(AccelerometerEventContract.COLUMN_NAME_Z, event.getAccelerationZ());
        insert(AccelerometerEventContract.CONTENT_URI, values);
    }

    @Override
    public void onBandBarometerChanged(final BandBarometerEvent event) {
        String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

        ContentValues values = new ContentValues();
        values.put(BarometerEventContract.COLUMN_TIME_STAMP, timeStamp);
        values.put(BarometerEventContract.COLUMN_NAME_AIR_PRESSURE, event.getAirPressure());
        values.put(BarometerEventContract.COLUMN_NAME_TEMPERATURE, event.getTemperature());
        insert(BarometerEventContract.CONTENT_URI, values);
    }

    @Override
    public void onBandDistanceChanged(final BandDistanceEvent event) {
        String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

        ContentValues values = new ContentValues();
        values.put(DistanceEventContract.COLUMN_TIME_STAMP, timeStamp);
        values.put(DistanceEventContract.COLUMN_NAME_MOTION_TYPE, event.getMotionType().toString());
        insert(DistanceEventContract.CONTENT_URI, values);
    }

    @Override
    public void onBandGsrChanged(BandGsrEvent event) {
        if (event != null) {
            String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

            ContentValues values = new ContentValues();
            values.put(GsrEventContract.COLUMN_TIME_STAMP, timeStamp);
            values.put(GsrEventContract.COLUMN_NAME_RESISTANCE, event.getResistance());
            insert(GsrEventContract.CONTENT_URI, values);
        }
    }

    @Override
    public void onBandGyroscopeChanged(final BandGyroscopeEvent event) {
        String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

        ContentValues values = new ContentValues();
        values.put(GyroscopeEventContract.COLUMN_TIME_STAMP, timeStamp);
        values.put(GyroscopeEventContract.COLUMN_NAME_ACCELERATION_X, event.getAccelerationX());
        values.put(GyroscopeEventContract.COLUMN_NAME_ACCELERATION_Y, event.getAccelerationY());
        values.put(GyroscopeEventContract.COLUMN_NAME_ACCELERATION_Z, event.getAccelerationZ());
        values.put(GyroscopeEventContract.COLUMN_NAME_ANGULAR_VELOCITY_X, event.getAngularVelocityX());
        values.put(GyroscopeEventContract.COLUMN_NAME_ANGULAR_VELOCITY_Y, event.getAngularVelocityY());
        values.put(GyroscopeEventContract.COLUMN_NAME_ANGULAR_VELOCITY_Z, event.getAngularVelocityZ());
        insert(GyroscopeEventContract.CONTENT_URI, values);
    }

    @Override
    public void onBandHeartRateChanged(final BandHeartRateEvent event) {
        if (event != null) {
            String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

            ContentValues values = new ContentValues();
            values.put(HeartRateEventContract.COLUMN_TIME_STAMP, timeStamp);
            values.put(HeartRateEventContract.COLUMN_HEART_RATE, event.getHeartRate());
            values.put(HeartRateEventContract.COLUMN_QUALITY, String.valueOf(event.getQuality()));
            insert(HeartRateEventContract.CONTENT_URI, values);
        }
    }

    @Override
    public void onBandRRIntervalChanged(BandRRIntervalEvent event) {
        if (event != null) {
            String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

            ContentValues values = new ContentValues();
            values.put(RRIntervalEventContract.COLUMN_TIME_STAMP, timeStamp);
            values.put(RRIntervalEventContract.COLUMN_NAME_RR_INTERVAL, event.getInterval());
           insert(RRIntervalEventContract.CONTENT_URI, values);
        }
    }

    @Override
    public void onBandSkinTemperatureChanged(BandSkinTemperatureEvent event) {
        if (event != null) {
            String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

            ContentValues values = new ContentValues();
            values.put(SkinTemperatureEventContract.COLUMN_TIME_STAMP, timeStamp);
            values.put(SkinTemperatureEventContract.COLUMN_NAME_TEMPERATURE, event.getTemperature());
            insert(SkinTemperatureEventContract.CONTENT_URI, values);
        }
    }
    @Override
    public void onBandUVChanged(final BandUVEvent event) {
        try {
            String timeStamp = DateTime.now(DateTimeZone.UTC).toString();

            ContentValues values = new ContentValues();
            values.put(UVEventContract.COLUMN_TIME_STAMP, timeStamp);
            values.put(UVEventContract.COLUMN_NAME_UV_EXPOSURE_TODAY, event.getUVExposureToday());
            values.put(UVEventContract.COLUMN_NAME_UV_INDEX_LEVEL, event.getUVIndexLevel().toString());
            insert(UVEventContract.CONTENT_URI, values);

        } catch (InvalidBandVersionException e) {
            Log.e(LOG_TAG, "Unable to retrieve UV data from Band", e);
        }
    }

}
