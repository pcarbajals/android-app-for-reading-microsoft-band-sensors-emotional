/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import io.veedata.auth.VeedataAccountService;

/**
 * Class for utility methods related to sync'ing to Veedata.
 *
 * Created by Pablo on 2/23/2016.
 */
public class SyncUtils {
    private static final String TAG = "SyncUtils";

    private static final String ACCOUNT_TYPE = "veedata.io";
    private static final String CONTENT_AUTHORITY = "io.veedata.data.provider";
    private static final String PREF_SETUP_COMPLETE = "setup_complete";
    private static final long SYNC_FREQUENCY = 60 * 60;  // 1 hour (in seconds)

    /**
     * Create an entry for this application in the system account list, if it isn't already there.
     *
     * @param context Context
     */
    public static Account CreateSyncAccount(Context context) {
        Log.i(TAG, "CreateSyncAccount");
        boolean newAccount = false;
        boolean setupComplete = PreferenceManager
                .getDefaultSharedPreferences(context).getBoolean(PREF_SETUP_COMPLETE, false);

        // Create account, if it's missing. (Either first run, or user has deleted account.)
        Account account = VeedataAccountService.GetAccount(ACCOUNT_TYPE);
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        if (accountManager.addAccountExplicitly(account, null, null)) {
            // Inform the system that this account supports sync
            ContentResolver.setIsSyncable(account, CONTENT_AUTHORITY, 1);
            // Inform the system that this account is eligible for auto sync when the network is up
            ContentResolver.setSyncAutomatically(account, CONTENT_AUTHORITY, true);
            // Recommend a schedule for automatic synchronization. The system may modify this based
            // on other scheduled syncs and network utilization.
            ContentResolver.addPeriodicSync(
                    account, CONTENT_AUTHORITY, new Bundle(),SYNC_FREQUENCY);
            newAccount = true;
        }

        // Schedule an initial sync if we detect problems with either our account or our local
        // data has been deleted. (Note that it's possible to clear app data WITHOUT affecting
        // the account list, so wee need to check both.)
        if (newAccount || !setupComplete) {
            TriggerRefresh();
            PreferenceManager.getDefaultSharedPreferences(context).edit()
                    .putBoolean(PREF_SETUP_COMPLETE, true).apply();
        }

        verifyDeviceId(context);

        return account;
    }

    private static void verifyDeviceId(final Context context) {
        Log.i(TAG, "Verifying Veedata device reference.");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String deviceId = preferences.getString("pref_veedata_deviceId", "");

        if (deviceId.isEmpty()) {
            Log.w(TAG, "Veedata device reference doesn't exist, creating one.");
            createVeedataDeviceId(context);
        }
    }

    /**
     * Helper method to trigger an immediate sync ("refresh").
     *
     * <p>This should only be used when we need to preempt the normal sync schedule. Typically, this
     * means the user has pressed the "refresh" button.
     *
     * Note that SYNC_EXTRAS_MANUAL will cause an immediate sync, without any optimization to
     * preserve battery life. If you know new data is available (perhaps via a GCM notification),
     * but the user is not actively waiting for that data, you should omit this flag; this will give
     * the OS additional freedom in scheduling your sync request.
     */
    public static void TriggerRefresh() {
        Log.i(TAG, "TriggerRefresh");
        Bundle b = new Bundle();
        // Disable sync backoff and ignore sync preferences. In other words...perform sync NOW!
        b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(
                VeedataAccountService.GetAccount(ACCOUNT_TYPE), // Sync account
                CONTENT_AUTHORITY,                 // Content authority
                b);                                             // Extras
    }

    static void createVeedataDeviceId(final Context context) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        Uri veedataUrl = getVeedataServerUrl(preferences);
        Uri.Builder urlBuilder = veedataUrl.buildUpon();

        urlBuilder.appendPath("devices/new");

        String alias = getVeedataDeviceAlias(preferences);
        if (!alias.isEmpty()) {
            urlBuilder.appendQueryParameter("alias", alias);
        }

        String patientId = getVeedataParticipantId(preferences);
        if (!patientId.isEmpty()) {
            urlBuilder.appendQueryParameter("userId", patientId);
        }

        Log.i(TAG, "Creating a new device reference in Veedata: " + urlBuilder.toString());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, urlBuilder.toString(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {
                try {
                    Log.d(TAG, "Response: " + response.toString());
                    SharedPreferences.Editor editor = preferences.edit();

                    final String deviceId = response.getString("id");
                    Log.i(TAG, "Setting device id to " + deviceId);
                    editor.putString("pref_veedata_deviceId", deviceId);

                    final String deviceAlias = response.getString("alias");
                    Log.i(TAG, "Setting device alias to " + deviceAlias);
                    editor.putString("pref_veedata_alias", deviceAlias);

                    final String currentUser = response.optString("currentUser");

                    Log.i(TAG, "Setting participant id to " + currentUser);
                    editor.putString("pref_veedata_participantId", currentUser);

                    editor.apply();

                } catch (JSONException e) {
                    Toast.makeText(context, "Error setting values from Veedata. Check log files.", Toast.LENGTH_LONG).show();
                    Log.e(TAG, "Unable to set settings from Veedata: " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
                Log.e(TAG, error.getMessage(), error);
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(30000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(request);
    }

    static String getVeedataDeviceId(final SharedPreferences preferences) {
        return preferences.getString("pref_veedata_deviceId", "");
    }

    static String getVeedataParticipantId(final SharedPreferences preferences) {
        return preferences.getString("pref_veedata_participantId", "");
    }

    private static String getVeedataDeviceAlias(final SharedPreferences preferences) {
        return preferences.getString("pref_veedata_alias", "");
    }

    static Uri getVeedataServerUrl(final SharedPreferences preferences) {
        return Uri.parse(preferences.getString("pref_veedata_host", "http://veedata-api-dev.azurewebsites.net/0.0.3"));
    }
}
