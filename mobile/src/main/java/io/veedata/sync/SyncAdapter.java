/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */
package io.veedata.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.google.common.base.CaseFormat;
import com.microsoft.azure.iothub.DeviceClient;
import com.microsoft.azure.iothub.IotHubClientProtocol;
import com.microsoft.azure.iothub.IotHubEventCallback;
import com.microsoft.azure.iothub.IotHubStatusCode;
import com.microsoft.azure.iothub.Message;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import io.veedata.veemotion.database.sensor.HeartRateEventContract;

/**
 * This SyncAdapter synchronizes Training data between the device and the cloud.
 * <p>
 * Created by Pablo on 2/21/2016.
 */
class SyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String TAG = "SyncAdapter";

    /**
     * Set up the sync adapter
     */
    SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, final ContentProviderClient provider, final SyncResult syncResult) {
        Log.i(TAG, "Beginning synchronization to Veedata");

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        final String userId = SyncUtils.getVeedataParticipantId(preferences);
        final String deviceId = SyncUtils.getVeedataDeviceId(preferences);

        if (deviceId.isEmpty()) {
            Log.e(TAG, "The Veedata device id is missing and can't perform a sync. Try again later.");
            syncResult.stats.numAuthExceptions++;
            syncResult.delayUntil = 30; // delay for 30 seconds

            Log.i(TAG, "Creating a new Veedata device id.");
            SyncUtils.createVeedataDeviceId(getContext());

            return;
        }

        Log.i(TAG, "onPerformSync: " + userId + ", " + deviceId);

        final Uri veedataUrl = SyncUtils.getVeedataServerUrl(preferences);
        syncSensorData(provider, syncResult, veedataUrl, deviceId);
    }

    private void syncSensorData(final ContentProviderClient provider, final SyncResult syncResult, final Uri veedataUrl, final String deviceId) {
        Uri.Builder urlBuilder = veedataUrl.buildUpon();
        urlBuilder.appendPath("devices/" + deviceId + "/iot-hub");

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Log.e(TAG, "Error finding out Veedata's IoT Hub information, will not sync: " + error.getMessage() + ". Network response: " + error.networkResponse.statusCode, error.fillInStackTrace());
            }
        };

        JsonObjectRequest request = new JsonObjectRequest(urlBuilder.toString(), null, future, errorListener);
        Volley.newRequestQueue(this.getContext()).add(request);

        try {
            Log.i(TAG, "Getting the IoT hub's info: " + urlBuilder.toString());
            JSONObject response = future.get(30, TimeUnit.SECONDS);
            Log.d(TAG, "Response: " + response.toString());
            syncToIoTHub(provider, syncResult, deviceId, response);

        } catch (InterruptedException | ExecutionException | TimeoutException error) {
            syncResult.stats.numAuthExceptions++;
        }
    }

    private void syncToIoTHub(final ContentProviderClient provider, final SyncResult syncResult, final String deviceId, final JSONObject response) {
        String connString = null;
        DeviceClient client = null;

        try {
            connString = response.getString("connString");
            client = new DeviceClient(connString, IotHubClientProtocol.MQTT);
            client.open();
            Log.i(TAG, "Connection to Veedata been opened.");

            JSONObject msgJson = new JSONObject();
            msgJson.put("deviceId", deviceId);

            msgJson.put("dataType", "sensor");
            msgJson.put("dataSubtype", HeartRateEventContract.TABLE_NAME.replace('_', '-'));
            final String[] projection = HeartRateEventContract.COLUMNS_TO_SYNC;
            sendContent(client, HeartRateEventContract.CONTENT_URI, projection, msgJson, provider, syncResult);

        } catch (IOException e) {
            Log.e(TAG, "Error sending messages to Veedata: " + e.getMessage() + ". Check the hostname, device id and shared access key and access to the internet.");
            Log.d(TAG, "connString=" + connString);

            syncResult.stats.numIoExceptions++;
            syncResult.delayUntil = 10800; // delay for 3 hours, the cloud service may be down.

        } catch (JSONException e) {
            Log.e(TAG, "Unable to send content. Error parsing an entry: " + e.getMessage(), e);
            syncResult.stats.numParseExceptions++;

        } catch (RemoteException e) {
            Log.e(TAG, "Error querying the content provider, sync won't continue: " + e.getMessage(), e);
            syncResult.databaseError = true;

        } catch (URISyntaxException e) {
            Log.e(TAG, "Error sending the event to Veedata: " + e.getMessage(), e);

            syncResult.stats.numIoExceptions++;
            syncResult.delayUntil = 10800; // delay for 3 hours, the Veedata service may be down.

        } finally {
            if (client != null) {
                try {
                    Log.i(TAG, "Closing connection to Veedata Cloud.");
                    client.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error closing the connection to IoT Hub: " + e.getMessage(), e);
                }
            }
        }
    }

    private void sendContent(final DeviceClient client, final Uri contentUri, final String[] projection, final JSONObject messageEnvelop, final ContentProviderClient provider, final SyncResult syncResult) throws RemoteException {
        Log.i(TAG, "Sync'ing " + contentUri);

        final String selectionQuery = HeartRateEventContract.COLUMN_SYNC_STATE + " = ?";
        final String[] selectionArgsQuery = {HeartRateEventContract.COLUMN_VALUE_BOOLEAN_FALSE};

        Cursor cursor = provider.query(contentUri, projection, selectionQuery, selectionArgsQuery, null);
        assert cursor != null;
        Log.i(TAG, "Entries to synchronize: " + cursor.getCount());

        cursor.moveToFirst();
        while (cursor.moveToNext()) {
            try {
                JSONObject data = new JSONObject();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    // do not send the record ID to the cloud
                    if (!cursor.getColumnName(i).equals(BaseColumns._ID)) {
                        final int type = cursor.getType(i);
                        final String jsonFieldName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, cursor.getColumnName(i));

                        if (type == Cursor.FIELD_TYPE_INTEGER) {
                            data.put(jsonFieldName, cursor.getInt(i));

                        } else if (type == Cursor.FIELD_TYPE_FLOAT) {
                            data.put(jsonFieldName, cursor.getFloat(i));

                        } else {
                            data.put(jsonFieldName, cursor.getString(i));
                        }
                    }
                }

                messageEnvelop.put("data", data);

                Message msg = new Message(messageEnvelop.toString());
                Log.v(TAG, "Sending the following event message: " + messageEnvelop.toString());

                IotHubEventCallback callback = new IotHubEventCallback() {
                    @Override
                    public void execute(final IotHubStatusCode responseStatus, final Object callbackContext) {
                        Log.d(TAG, "Veedata Cloud response: status " + responseStatus + " and callback context " + callbackContext);

                        if (responseStatus == IotHubStatusCode.OK || responseStatus == IotHubStatusCode.OK_EMPTY) {
                            final String entryId = callbackContext.toString();
                            Log.d(TAG, "Entry " + entryId + " successfully synchronized");
                            try {
                                final ContentValues values = new ContentValues();
                                values.put(HeartRateEventContract.COLUMN_SYNC_STATE, HeartRateEventContract.COLUMN_VALUE_BOOLEAN_TRUE);
                                final String selectionUpdate = HeartRateEventContract._ID + " = ?";
                                final String[] selectionArgsUpdate = {entryId};
                                provider.update(contentUri, values, selectionUpdate, selectionArgsUpdate);
                                syncResult.stats.numEntries++;

                            } catch (RemoteException e) {
                                Log.e(TAG, "Unable to update entry " + entryId + " as synchronized: " + e.getMessage(), e);
                                syncResult.databaseError = true;
                            }
                        }
                    }
                };
                final int primaryKeyColumnIndex = cursor.getColumnIndex(BaseColumns._ID);
                final String primaryKey = cursor.getString(primaryKeyColumnIndex);

                // TODO return the message instead of sending the event by pulling out the callback inner class and obtaining the primary key directly from the message
                client.sendEventAsync(msg, callback, primaryKey);

            } catch (JSONException e) {
                Log.e(TAG, "Error parsing an entry: " + e.getMessage(), e);
                syncResult.stats.numParseExceptions++;
                syncResult.stats.numSkippedEntries++;
            }
        }

        cursor.close();
    }

}
