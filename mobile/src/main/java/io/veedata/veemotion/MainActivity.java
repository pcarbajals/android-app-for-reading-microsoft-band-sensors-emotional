/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import io.veedata.band.BandConnectionManager;
import io.veedata.band.sensor.SensorListenerTaskResult;
import io.veedata.sync.SyncUtils;
import io.veedata.veemotion.database.sensor.HeartRateEventContract;
import io.veedata.veemotion.view.DailyHeartRateCollectionData;
import io.veedata.veemotion.view.DashboardCardData;
import io.veedata.veemotion.view.DashboardRecyclerViewAdapter;
import io.veedata.veemotion.view.SensorReading;
import io.veedata.veemotion.view.WeeklyHeartRateData;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    /**
     * TextView for any status messages.
     */
    private TextView mStatusTextView;

    //The BroadcastReceiver that listens for bluetooth broadcasts
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                final String message = "Bluetooth device " + device.getName() + " has been found.";
                Log.i(LOG_TAG, message);
//                displayStatus(message);

            } else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                final String message = "Bluetooth device " + device.getName() + " is now connected.";
                Log.i(LOG_TAG, message);
//                displayStatus(message);

            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                final String message = "Bluetooth device " + device.getName() + " discovery finished.";
                Log.i(LOG_TAG, message);
//                displayStatus(message);

            } else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
                final String message = "Bluetooth device " + device.getName() + " is about to disconnect.";
                Log.w(LOG_TAG, message);
//                displayStatus(message);

            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                final String message = "Bluetooth device " + device.getName() + " has disconnected.";
                Log.w(LOG_TAG, message);
//                displayStatus(message);
            }
        }
    };

    public void displayStatus(final String message) {
        mStatusTextView.setText(message);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        final StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1,
                StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);

        List<DashboardCardData> sList = getCardDataList();

        DashboardRecyclerViewAdapter rcAdapter = new DashboardRecyclerViewAdapter(
                sList);
        recyclerView.setAdapter(rcAdapter);

        mStatusTextView = (TextView) findViewById(R.id.statusTextView);

        // Create the default account for the Veedata SyncService
        SyncUtils.CreateSyncAccount(this);

    }

    private List<DashboardCardData> getCardDataList() {
        List<DashboardCardData> listViewItems = new ArrayList<DashboardCardData>();
        listViewItems.add(new SensorReading("Beats per minute", R.mipmap.ic_heart_black_36dp, HeartRateEventContract.CONTENT_URI, HeartRateEventContract.COLUMN_HEART_RATE));
        listViewItems.add(new DailyHeartRateCollectionData("Sensor and Band State", HeartRateEventContract.CONTENT_URI, HeartRateEventContract.COLUMN_HEART_RATE));
        listViewItems.add(new WeeklyHeartRateData("Daily Heart Rate Average", HeartRateEventContract.CONTENT_URI, HeartRateEventContract.COLUMN_HEART_RATE));

        return listViewItems;
    }

    @Override
    protected void onStart() {
        Log.i(LOG_TAG, "onStart()");
        super.onStart();

        registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(LOG_TAG, "onResume()");

        Log.i(LOG_TAG, "Asking the SensorListenerTask to start listening to Band sensors");
        final WeakReference<Activity> reference = new WeakReference<Activity>(this);
        new SensorListenerTask().execute(reference);
    }

    @Override
    protected void onStop() {
        unregisterReceiver(mReceiver);

        super.onStop();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_sync) {
            Log.i(LOG_TAG, "Navigation item 'sync' selected.");
            SyncUtils.TriggerRefresh();

        } else if (id == R.id.nav_questionnaire) {
            final int questionnaireId = 1;
            final int numberOfQuestions = 6;

            final Intent questionnaireIntent = new Intent(this, QuestionnaireActivity.class);
            questionnaireIntent.putExtra(QuestionnaireActivity.ARG_QUESTIONNAIRE_ID, questionnaireId);
            questionnaireIntent.putExtra(QuestionnaireActivity.ARG_TOTAL_QUESTIONS, numberOfQuestions);

            startActivity(questionnaireIntent);

        } else if (id == R.id.nav_diary) {
            final int questionId = 2;
            final int numberOfQuestions = 6;

            final Intent diaryIntent = new Intent(this, QuestionnaireActivity.class);
            diaryIntent.putExtra(QuestionnaireActivity.ARG_QUESTIONNAIRE_ID, questionId);
            diaryIntent.putExtra(QuestionnaireActivity.ARG_TOTAL_QUESTIONS, numberOfQuestions);

            startActivity(diaryIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class SensorListenerTask extends AsyncTask<WeakReference<Activity>, Void, SensorListenerTaskResult> {

        private WeakReference<Activity> mActivityWeakReference;

        @Override
        protected SensorListenerTaskResult doInBackground(WeakReference<Activity>... params) {
            mActivityWeakReference = params[0];
            return BandConnectionManager.getInstance().startRecording(mActivityWeakReference);
        }

        @Override
        protected void onPostExecute(final SensorListenerTaskResult sensorListenerTaskResult) {
            Log.i(LOG_TAG, sensorListenerTaskResult.getMessage());

            if (sensorListenerTaskResult.isConnected()) {
                Log.i(LOG_TAG, "Band is connected, listening to sensors.");
            }
        }
    }
}
