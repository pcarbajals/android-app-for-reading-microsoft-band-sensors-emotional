/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import io.veedata.veemotion.view.QuestionnairePageFragment;

/**
 * An activity class for the in-promptu and diary questionnaires.
 */
public class QuestionnaireActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    /**
     * The bundle argument representing the total number of questions that this activity will use.
     */
    public static final String ARG_TOTAL_QUESTIONS = "question_total";

    /**
     * The bundle argument representing the questionnaire id that this activity will use.
     */
    public static final String ARG_QUESTIONNAIRE_ID = "questionnaire_id";

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private QuestionsPagerAdapter mQuestionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    /**
     * The {@link ImageButton} for going to the previous question
     */
    private ImageButton mBackButton;

    /**
     * The {@link ImageButton} for going to the next question. This button gets replaced by the
     * finish button on the last question.
     */
    private ImageButton mNextButton;

    /**
     * The {@link Button} to complete the questionnaire. It shows up only on the last question.
     */
    private Button mFinishButton;
    private ImageView[] mProgressIndicators;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);

        final int questionnaireId = getIntent().getIntExtra(ARG_QUESTIONNAIRE_ID, 0);
        final int numberOfQuestions = getIntent().getIntExtra(ARG_TOTAL_QUESTIONS, 0);

        // Create the adapter that will return a fragment for each of question in the questionnaire
        mQuestionsPagerAdapter = new QuestionsPagerAdapter(getSupportFragmentManager(), questionnaireId, numberOfQuestions);

        // Set up the ViewPager with the questions adapter
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mQuestionsPagerAdapter);
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setCurrentItem(0);

        // back button is initially invisible
        mBackButton = (ImageButton) findViewById(R.id.button_back);
        mBackButton.setVisibility(View.GONE);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final int currentQuestion = mViewPager.getCurrentItem();
                mViewPager.setCurrentItem(currentQuestion - 1);
            }
        });

        mNextButton = (ImageButton) findViewById(R.id.button_next);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final int currentQuestion = mViewPager.getCurrentItem();
                mViewPager.setCurrentItem(currentQuestion + 1);
            }
        });

        mFinishButton = (Button) findViewById(R.id.button_finish);
        mFinishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                saveQuestionnaireResponses();
                finish();
            }
        });

        createProgressIndicators();
    }

    private void saveQuestionnaireResponses() {
        // TODO: save responses to database
    }

    /**
     * Creates the progress indicators that show at the bottom of the screen. The number of
     * indicators corresponds to the number of questions. At any given time, only one indicator will
     * be drawn as selected, indicating the currently displayed question.
     */
    private void createProgressIndicators() {
        final LinearLayout container = (LinearLayout) findViewById(R.id.progress_indicators_container);
        final int numberOfQuestions = mQuestionsPagerAdapter.getCount();
        mProgressIndicators = new ImageView[numberOfQuestions];

        // Add as many progress indicators as available questions
        for (int count = 0; count < numberOfQuestions; count++) {
            final ImageView progressIndicator = new ImageView(this);

            if (count == 0) {
                progressIndicator.setBackground(getResources().getDrawable(R.drawable.indicator_selected, getTheme()));

            } else {
                progressIndicator.setBackground(getResources().getDrawable(R.drawable.indicator_unselected, getTheme()));
            }

            final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(30, 30);
            layoutParams.setMargins(20, 0, 20, 0);
            container.addView(progressIndicator, layoutParams);

            mProgressIndicators[count] = progressIndicator;
        }
    }

    @Override
    public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
        // do nothing
    }

    @Override
    public void onPageSelected(final int position) {
        // change the progress indicator image depending on the page currently selected
        for (int i = 0; i < mProgressIndicators.length; i++) {
            mProgressIndicators[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }

        // disable the back button if the current page is the first question
        if (position == 0) {
            mBackButton.setVisibility(View.GONE);
        } else {
            mBackButton.setVisibility(View.VISIBLE);
        }

        // swap the next button by the finish button
        final int lastQuestion = mQuestionsPagerAdapter.getCount() - 1;
        mNextButton.setVisibility(position == lastQuestion ? View.GONE : View.VISIBLE);
        mFinishButton.setVisibility(position == lastQuestion ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onPageScrollStateChanged(final int state) {
        // do nothing
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class QuestionsPagerAdapter extends FragmentPagerAdapter {

        private final int mQuestionnaireId;

        /**
         * The number of pages. Each page holds one question, therefore this field also represent
         * the total number of questions.
         */
        private final int mNumberOfPages;

        QuestionsPagerAdapter(FragmentManager fm, final int questionnaireId, final int numberOfPages) {
            super(fm);
            mQuestionnaireId = questionnaireId;
            mNumberOfPages = numberOfPages;
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return QuestionnairePageFragment.newInstance(mQuestionnaireId, position + 1);
        }

        @Override
        public int getCount() {
            return mNumberOfPages;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Question " + (position + 1);
        }
    }
}
