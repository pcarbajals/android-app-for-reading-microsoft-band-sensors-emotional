/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database.sensor;

import android.net.Uri;

/**
 * Created by Pablo on 4/13/2016.
 */
public class AccelerometerEventContract extends BandDataContract {

    public static final String TABLE_NAME = "accelerometer";

    /**
     * Fully qualified URI for "entry" resources.
     */
    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    /**
     * Name for column "x-axis" of type DOUBLE
     */
    public static final String COLUMN_NAME_X = "X";

    /**
     * Name for column "y-axis" of type DOUBLE
     */
    public static final String COLUMN_NAME_Y = "Y";

    /**
     * Name for column "z-axis" of type DOUBLE
     */
    public static final String COLUMN_NAME_Z = "Z";

    public static final String SQL_CREATE_ACCELEROMETER_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_TIME_STAMP + " TEXT," +
                    COLUMN_NAME_X + " REAL," +
                    COLUMN_NAME_Y + " REAL," +
                    COLUMN_NAME_Z + " REAL," +
                    COLUMN_SYNC_STATE + " INTEGER DEFAULT " + COLUMN_VALUE_BOOLEAN_FALSE + ")";

    public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
