/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Arrays;

import io.veedata.veemotion.database.sensor.AccelerometerEventContract;
import io.veedata.veemotion.database.sensor.AmbientLightEventContract;
import io.veedata.veemotion.database.sensor.BarometerEventContract;
import io.veedata.veemotion.database.sensor.ConnectionStateEventContract;
import io.veedata.veemotion.database.sensor.ContactStateEventContract;
import io.veedata.veemotion.database.sensor.DistanceEventContract;
import io.veedata.veemotion.database.sensor.GsrEventContract;
import io.veedata.veemotion.database.sensor.GyroscopeEventContract;
import io.veedata.veemotion.database.sensor.HeartRateEventContract;
import io.veedata.veemotion.database.sensor.RRIntervalEventContract;
import io.veedata.veemotion.database.sensor.SkinTemperatureEventContract;
import io.veedata.veemotion.database.sensor.UVEventContract;

/**
 * Created by great_000 on 2/1/2016.
 */
public class VeemotionDataProvider extends ContentProvider {

    private static final String LOG_TAG = VeemotionDataProvider.class.getSimpleName();

    // Enables the use of 'limit' and 'offset' in queries
    public static final String QUERY_PARAMETER_LIMIT = "limit";

    /**
     * UriMatcher, used to decode incoming URIs.
     */
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private static final int ROUTE_HEART_RATE = 1;
    private static final int ROUTE_RR_INTERVAL = 2;
    private static final int ROUTE_GSR = 3;
    private static final int ROUTE_SKIN_TEMPERATURE = 4;
    private static final int ROUTE_CONTACT_STATE = 5;
    private static final int ROUTE_CONNECTION_STATE = 6;
    private static final int ROUTE_ACCELEROMETER = 7;
    private static final int ROUTE_AMBIENT_LIGHT = 8;
    private static final int ROUTE_BAROMETER = 9;
    private static final int ROUTE_DISTANCE = 10;
    private static final int ROUTE_GYROSCOPE = 11;
    private static final int ROUTE_UV = 12;
    private static final int ROUTE_HR_HISTORY = 13;

    static {
        sUriMatcher.addURI(AccelerometerEventContract.CONTENT_AUTHORITY, AccelerometerEventContract.TABLE_NAME, ROUTE_ACCELEROMETER);
        sUriMatcher.addURI(AmbientLightEventContract.CONTENT_AUTHORITY, AmbientLightEventContract.TABLE_NAME, ROUTE_AMBIENT_LIGHT);
        sUriMatcher.addURI(BarometerEventContract.CONTENT_AUTHORITY, BarometerEventContract.TABLE_NAME, ROUTE_BAROMETER);
        sUriMatcher.addURI(DistanceEventContract.CONTENT_AUTHORITY, DistanceEventContract.TABLE_NAME, ROUTE_DISTANCE);
        sUriMatcher.addURI(GsrEventContract.CONTENT_AUTHORITY, GsrEventContract.TABLE_NAME, ROUTE_GSR);
        sUriMatcher.addURI(GyroscopeEventContract.CONTENT_AUTHORITY, GyroscopeEventContract.TABLE_NAME, ROUTE_GYROSCOPE);
        sUriMatcher.addURI(HeartRateEventContract.CONTENT_AUTHORITY, HeartRateEventContract.TABLE_NAME, ROUTE_HEART_RATE);
        sUriMatcher.addURI(RRIntervalEventContract.CONTENT_AUTHORITY, RRIntervalEventContract.TABLE_NAME, ROUTE_RR_INTERVAL);
        sUriMatcher.addURI(SkinTemperatureEventContract.CONTENT_AUTHORITY, SkinTemperatureEventContract.TABLE_NAME, ROUTE_SKIN_TEMPERATURE);
        sUriMatcher.addURI(ContactStateEventContract.CONTENT_AUTHORITY, ContactStateEventContract.TABLE_NAME, ROUTE_CONTACT_STATE);
        sUriMatcher.addURI(ConnectionStateEventContract.CONTENT_AUTHORITY, ConnectionStateEventContract.TABLE_NAME, ROUTE_CONNECTION_STATE);
        sUriMatcher.addURI(UVEventContract.CONTENT_AUTHORITY, UVEventContract.TABLE_NAME, ROUTE_UV);
    }

    private SQLiteOpenHelper mMicrosoftBandDatabaseHelper;

    /*
     * Always return true, indicating that the
     * provider loaded correctly.
     */
    @Override
    public boolean onCreate() {
        mMicrosoftBandDatabaseHelper = new VeemotionDatabase(getContext());

        return true;
    }

    /*
     * Return no type for MIME type
     */
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    /*
     * query() always returns no results
     *
     */
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.v(LOG_TAG, "query() ENTER uri=" + uri + ", projection=" + Arrays.toString(projection) + ", selection=" + selection + ", selectionArgs=" + Arrays.toString(selectionArgs) + ", sortOrder=" + sortOrder);

        String limit = uri.getQueryParameter(QUERY_PARAMETER_LIMIT);

        final SQLiteDatabase db = mMicrosoftBandDatabaseHelper.getWritableDatabase();
        assert db != null;

        Cursor cursor = null;
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_ACCELEROMETER:
            case ROUTE_AMBIENT_LIGHT:
            case ROUTE_BAROMETER:
            case ROUTE_DISTANCE:
            case ROUTE_GSR:
            case ROUTE_GYROSCOPE:
            case ROUTE_HEART_RATE:
            case ROUTE_RR_INTERVAL:
            case ROUTE_SKIN_TEMPERATURE:
            case ROUTE_UV:
            case ROUTE_HR_HISTORY:
            case ROUTE_CONTACT_STATE:
            case ROUTE_CONNECTION_STATE:
                final String lastPathSegment = uri.getLastPathSegment();
                cursor = db.query(lastPathSegment, projection, selection, selectionArgs, null, null, sortOrder, limit);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // Note: Notification URI must be manually set here for loaders to correctly
        // register ContentObservers.
        Context ctx = getContext();
        if (ctx == null) throw new AssertionError();
        if (cursor == null) throw new AssertionError();
        cursor.setNotificationUri(ctx.getContentResolver(), uri);

        return cursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        Log.v(LOG_TAG, "Inserting " + Arrays.toString(values.keySet().toArray(new String[0])) + " to uri: " + uri);

        final SQLiteDatabase db = mMicrosoftBandDatabaseHelper.getWritableDatabase();
        assert db != null;

        long id;
        Uri result;
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_ACCELEROMETER:
            case ROUTE_AMBIENT_LIGHT:
            case ROUTE_BAROMETER:
            case ROUTE_DISTANCE:
            case ROUTE_GSR:
            case ROUTE_GYROSCOPE:
            case ROUTE_HEART_RATE:
            case ROUTE_RR_INTERVAL:
            case ROUTE_SKIN_TEMPERATURE:
            case ROUTE_CONTACT_STATE:
            case ROUTE_CONNECTION_STATE:
            case ROUTE_UV:
                final String lastPathSegment = uri.getLastPathSegment();
                id = db.insertOrThrow(lastPathSegment, null, values);
                result = Uri.parse(uri + "/" + id);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // Send broadcast to registered ContentObservers, to refresh UI.
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return result;
    }

    /*
     * delete() always returns "no rows affected" (0)
     */
    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    /*
     * This method handles only ROUTE_STREAM_SESSION, all other updates will throw an UnsupportedOperationException
     */
    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.v(LOG_TAG, "Updating to uri: " + uri);

        final SQLiteDatabase db = mMicrosoftBandDatabaseHelper.getWritableDatabase();
        assert db != null;

        int rowsAffected;
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_ACCELEROMETER:
            case ROUTE_AMBIENT_LIGHT:
            case ROUTE_BAROMETER:
            case ROUTE_DISTANCE:
            case ROUTE_GSR:
            case ROUTE_GYROSCOPE:
            case ROUTE_HEART_RATE:
            case ROUTE_RR_INTERVAL:
            case ROUTE_SKIN_TEMPERATURE:
            case ROUTE_UV:
                final String lastPathSegment = uri.getLastPathSegment();
                rowsAffected = db.update(lastPathSegment, values, selection, selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        return rowsAffected;
    }

}