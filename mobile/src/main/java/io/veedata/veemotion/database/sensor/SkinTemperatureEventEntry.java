/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database.sensor;

/**
 * Created by Pablo on 1/27/2016.
 */
public class SkinTemperatureEventEntry extends SensorEventEntry {
    private float temperature;

    public SkinTemperatureEventEntry(String eventTimeStamp, float temperature) {
        super(eventTimeStamp);
        this.temperature = temperature;
    }

    public float getTemperature() {
        return temperature;
    }

}
