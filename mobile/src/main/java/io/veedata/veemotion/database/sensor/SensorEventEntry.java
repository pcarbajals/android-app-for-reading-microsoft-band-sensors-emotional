/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database.sensor;

/**
 * Created by Pablo on 2/1/2016.
 */
public abstract class SensorEventEntry {
    public String id;
    protected String eventTimestamp;

    public SensorEventEntry(String eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    public String getEventTimestamp() {
        return eventTimestamp;
    }
}
