/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database.sensor;

import android.net.Uri;

/**
 * Created by great_000 on 2/3/2016.
 */
public class GsrEventContract extends BandDataContract {

    public static final String TABLE_NAME = "gsr";

    public static final String COLUMN_NAME_RESISTANCE = "resistance";

    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    public static final String SQL_CREATE_GSR_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_TIME_STAMP + " TEXT," +
                    COLUMN_NAME_RESISTANCE + " INTEGER," +
                    COLUMN_SYNC_STATE + " INTEGER DEFAULT " + COLUMN_VALUE_BOOLEAN_FALSE + ")";

    public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
