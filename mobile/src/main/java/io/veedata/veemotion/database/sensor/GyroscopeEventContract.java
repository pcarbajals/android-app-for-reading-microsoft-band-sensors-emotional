/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database.sensor;

import android.net.Uri;

/**
 * Created by great_000 on 4/13/2016.
 */
public class GyroscopeEventContract extends BandDataContract {
    public static final String TABLE_NAME = "gyroscope";

    public static final String COLUMN_NAME_ACCELERATION_X = "acceleration_x";
    public static final String COLUMN_NAME_ACCELERATION_Y = "acceleration_y";
    public static final String COLUMN_NAME_ACCELERATION_Z = "acceleration_z";

    public static final String COLUMN_NAME_ANGULAR_VELOCITY_X = "ang_velocity_x";
    public static final String COLUMN_NAME_ANGULAR_VELOCITY_Y = "ang_velocity_y";
    public static final String COLUMN_NAME_ANGULAR_VELOCITY_Z = "ang_velocity_z";

    public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    public static final String SQL_CREATE_GYROSCOPE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_TIME_STAMP + " TEXT," +
                    COLUMN_NAME_ACCELERATION_X + " REAL," +
                    COLUMN_NAME_ACCELERATION_Y + " REAL," +
                    COLUMN_NAME_ACCELERATION_Z + " REAL," +
                    COLUMN_NAME_ANGULAR_VELOCITY_X + " REAL," +
                    COLUMN_NAME_ANGULAR_VELOCITY_Y + " REAL," +
                    COLUMN_NAME_ANGULAR_VELOCITY_Z + " REAL," +
                    COLUMN_SYNC_STATE + " INTEGER DEFAULT " + COLUMN_VALUE_BOOLEAN_FALSE + ")";

    public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
