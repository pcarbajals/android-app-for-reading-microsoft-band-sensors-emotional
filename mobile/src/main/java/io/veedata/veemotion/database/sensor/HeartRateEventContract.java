/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database.sensor;

import android.net.Uri;

/**
 * Created by Pablo on 2/2/2016.
 */
public class HeartRateEventContract extends BandDataContract {

    public static final String TABLE_NAME = "heart_rate";

    /**
     * Fully qualified URI for "entry" resources.
     */
    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    /**
     * Name for column "heart_rate" of type INTEGER
     */
    public static final String COLUMN_HEART_RATE = "heart_rate";

    /**
     * Name for column "quality" of type TEXT
     */
    public static final String COLUMN_QUALITY = "quality";
    public static final String COLUMN_QUALITY_VALUE_ACQUIRING = "ACQUIRING";
    public static final String COLUMN_QUALITY_VALUE_LOCKED = "LOCKED";

    public static final String SQL_CREATE_HEART_RATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_TIME_STAMP + " TEXT," +
                    COLUMN_HEART_RATE + " INTEGER," +
                    COLUMN_QUALITY + " TEXT," +
                    COLUMN_SYNC_STATE + " INTEGER DEFAULT " + COLUMN_VALUE_BOOLEAN_FALSE + ")";

    public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static final String[] COLUMNS_TO_SYNC = {_ID, COLUMN_TIME_STAMP, COLUMN_HEART_RATE, COLUMN_QUALITY};
}
