/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import io.veedata.veemotion.database.sensor.AccelerometerEventContract;
import io.veedata.veemotion.database.sensor.AmbientLightEventContract;
import io.veedata.veemotion.database.sensor.BarometerEventContract;
import io.veedata.veemotion.database.sensor.ConnectionStateEventContract;
import io.veedata.veemotion.database.sensor.ContactStateEventContract;
import io.veedata.veemotion.database.sensor.DistanceEventContract;
import io.veedata.veemotion.database.sensor.GsrEventContract;
import io.veedata.veemotion.database.sensor.GyroscopeEventContract;
import io.veedata.veemotion.database.sensor.HeartRateEventContract;
import io.veedata.veemotion.database.sensor.RRIntervalEventContract;
import io.veedata.veemotion.database.sensor.SkinTemperatureEventContract;
import io.veedata.veemotion.database.sensor.UVEventContract;

/**
 * Database for sensor data and questionnaire responses.
 *
 * Created by Pablo A. Carbajal on 4/13/2016.
 */
public class VeemotionDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "veemotion.sql";

    public VeemotionDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create tables for sensor data
        db.execSQL(AccelerometerEventContract.SQL_CREATE_ACCELEROMETER_TABLE);
        db.execSQL(AmbientLightEventContract.SQL_CREATE_AMBIENT_LIGHT_TABLE);
        db.execSQL(BarometerEventContract.SQL_CREATE_BAROMETER_TABLE);
        db.execSQL(DistanceEventContract.SQL_CREATE_DISTANCE_TABLE);
        db.execSQL(GsrEventContract.SQL_CREATE_GSR_TABLE);
        db.execSQL(GyroscopeEventContract.SQL_CREATE_GYROSCOPE_TABLE);
        db.execSQL(HeartRateEventContract.SQL_CREATE_HEART_RATE_TABLE);
        db.execSQL(RRIntervalEventContract.SQL_CREATE_RR_INTERVAL_TABLE);
        db.execSQL(SkinTemperatureEventContract.SQL_CREATE_SKIN_TEMPERATURE_TABLE);
        db.execSQL(ContactStateEventContract.SQL_CREATE_CONTACT_STATE_TABLE);
        db.execSQL(ConnectionStateEventContract.SQL_CREATE_CONNECTION_STATE_TABLE);
        db.execSQL(UVEventContract.SQL_CREATE_UV_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropAllAndRecreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropAllAndRecreate(db);
    }

    private void dropAllAndRecreate(SQLiteDatabase db) {
        // drop tables for sensor data
        db.execSQL(AccelerometerEventContract.SQL_DROP_TABLE);
        db.execSQL(AmbientLightEventContract.SQL_DROP_TABLE);
        db.execSQL(BarometerEventContract.SQL_DROP_TABLE);
        db.execSQL(DistanceEventContract.SQL_DROP_TABLE);
        db.execSQL(GsrEventContract.SQL_DROP_TABLE);
        db.execSQL(GyroscopeEventContract.SQL_DROP_TABLE);
        db.execSQL(HeartRateEventContract.SQL_DROP_TABLE);
        db.execSQL(RRIntervalEventContract.SQL_DROP_TABLE);
        db.execSQL(SkinTemperatureEventContract.SQL_DROP_TABLE);
        db.execSQL(ContactStateEventContract.SQL_DROP_TABLE);
        db.execSQL(ConnectionStateEventContract.SQL_DROP_TABLE);
        db.execSQL(UVEventContract.SQL_DROP_TABLE);

        // recreate all tables
        onCreate(db);
    }

}
