/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database.sensor;

import io.veedata.veemotion.database.BaseDataContract;

/**
 * Created by Pablo on 4/18/2016.
 */
public abstract class BandDataContract extends BaseDataContract {

    /**
     * Name for column "time_stamp" of type TEXT
     */
    public static final String COLUMN_TIME_STAMP = "timestamp";
    public static final String COLUMN_SYNC_STATE = "sync_state";

}
