/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database.sensor;

import android.net.Uri;

/**
 * Created by Pablo on 4/13/2016.
 */
public class UVEventContract extends BandDataContract {
    public static String TABLE_NAME = "uv";

    public static final String COLUMN_NAME_UV_INDEX_LEVEL = "uv_index_level";
    public static final String COLUMN_NAME_UV_EXPOSURE_TODAY = "uv_exposure_today";

    public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    public static final String SQL_CREATE_UV_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_TIME_STAMP + " TEXT," +
                    COLUMN_NAME_UV_EXPOSURE_TODAY + " INTEGER," +
                    COLUMN_NAME_UV_INDEX_LEVEL + " TEXT," +
                    COLUMN_SYNC_STATE + " INTEGER DEFAULT " + COLUMN_VALUE_BOOLEAN_FALSE + ")";

    public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
