/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database.sensor;

import android.net.Uri;

/**
 * Created by Pablo on 3/9/2016.
 */
public class ConnectionStateEventContract extends BandDataContract {
    public static String TABLE_NAME = "connection_state";

    public static final String COLUMN_NAME_STATE = "state";

    public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    public static final String SQL_CREATE_CONNECTION_STATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_TIME_STAMP + " TEXT," +
                    COLUMN_NAME_STATE + " TEXT," +
                    COLUMN_SYNC_STATE + " INTEGER DEFAULT " + COLUMN_VALUE_BOOLEAN_FALSE + ")";

    public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
