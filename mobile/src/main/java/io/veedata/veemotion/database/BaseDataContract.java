/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.database;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by great_000 on 11/7/2016.
 */
public class BaseDataContract implements BaseColumns {
    /**
     * Content provider authority.
     */
    public static final String CONTENT_AUTHORITY = "io.veedata.band.provider";
    public static final String COLUMN_VALUE_BOOLEAN_FALSE = "0";
    public static final String COLUMN_VALUE_BOOLEAN_TRUE = "1";
    /**
     * Base URI. (content://io.veedata.android.band.provider)
     */
    protected static final Uri BASE_CONTENT_URI = Uri.parse("content://" + BaseDataContract.CONTENT_AUTHORITY);
}
