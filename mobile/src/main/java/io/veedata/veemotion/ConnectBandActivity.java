/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import io.veedata.band.BandConnectionManager;
import io.veedata.band.sensor.SensorListenerTaskResult;

public class ConnectBandActivity extends AppCompatActivity {

    private static final String LOG_TAG = ConnectBandActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connectband);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();

/*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        if (BandConnectionManager.isConnectedToBand()) {
            Log.i(LOG_TAG, "Band is connected, launching MainActivity");
            startActivity(new Intent(this, MainActivity.class));

        } else if (BandConnectionManager.hasPairedBands()) {
            Log.i(LOG_TAG, "This device has paired Bands but user needs to add a Band.");

            TextView textView = (TextView) findViewById(R.id.text_view);
            textView.setText("Press the '+' button to connect to a Microsoft Band");

            // else - not paired - ask to pair
        } else {
            Log.i(LOG_TAG, "No Microsoft Band has been paired to this device.");
            TextView textView = (TextView) findViewById(R.id.text_view);
            textView.setText("No Microsoft Band has been paired to this device.\n\n First, pair a Band via the Microsoft Health app,\n then come back to this app.");

            fab.setVisibility(View.INVISIBLE);
        }
*/
    }
}
