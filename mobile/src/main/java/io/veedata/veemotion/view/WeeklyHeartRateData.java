/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.net.Uri;

import io.veedata.veemotion.view.DashboardCardData;

/**
 * Created by great_000 on 9/23/2016.
 */
public class WeeklyHeartRateData extends DashboardCardData {

    public WeeklyHeartRateData(final String label, final Uri contentUri, final String contentDataId) {
        super(label, contentUri, contentDataId);
    }
}
