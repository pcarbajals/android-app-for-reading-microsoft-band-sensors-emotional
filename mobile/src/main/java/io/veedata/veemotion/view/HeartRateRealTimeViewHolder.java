/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import io.veedata.veemotion.R;

/**
 * Created by great_000 on 9/23/2016.
 */

public class HeartRateRealTimeViewHolder extends CardViewHolder {
    private static final String LOG_TAG = HeartRateRealTimeViewHolder.class.getName();
    private final ImageView mImage;
    private final TextView mValue;

    /**
     * Scheduler for executing the command mUpdateCommand for updating the timer UI
     */
    private Handler mUpdateScheduler = new Handler();

    private boolean mIsTaskRunning = false;

    /**
     * This command updates the UI based on a {@link Handler} such as mUpdateScheduler
     */
    private Runnable mUpdateCommand = new Runnable() {

        @Override
        public void run() {
            mValue.setText(getLastEntry());
            mUpdateScheduler.postDelayed(this, 500);
        }
    };

    public HeartRateRealTimeViewHolder(final View cardView) {
        super(cardView);

        mValue = (TextView) cardView.findViewById(R.id.value);
        mImage = (ImageView) cardView.findViewById(R.id.image);
    }

    private String getLastEntry() {
        String heartRate = "-";

        /*
         * To get the last inserted row, we need to execute the query
         *    "SELECT * FROM table ORDER BY column DESC LIMIT 1;"
         *
         * Because we don't have the WHERE, the selection is null and no selectionArgs.
         * However to specify "LIMIT 1" in the query, we need to build a new URI with the parameter 'limit=1'
         */
        final String[] projection = {getColumnName()};
        final String selection = null;
        final String[] selectionArgs = {};
        final String sortOrder = BaseColumns._ID + " DESC";

        final Uri uri = getContentUri().buildUpon().appendQueryParameter("limit", "1").build();

        Cursor query = mValue.getContext().getContentResolver().query(uri, projection, selection, selectionArgs, sortOrder);

        if (query.getCount() > 0) {
            query.moveToFirst();
            heartRate = Integer.toString(query.getInt(0));
        }

        query.close();

        Log.v(LOG_TAG, "getLastEntry() RETURN " + heartRate);
        return heartRate;
    }

    @Override
    public void bindData(final DashboardCardData data) {
        super.bindData(data);
        start();
    }

    /**
     * Starts ticking the timer text view with now as the starting time.
     */
    private void start() {
        mValue.setText(getLastEntry());

        mIsTaskRunning = true;
        mUpdateScheduler.postDelayed(mUpdateCommand, 0);
    }

    public void stop() {
        mIsTaskRunning = false;
        mUpdateScheduler.removeCallbacks(mUpdateCommand);
    }
}
