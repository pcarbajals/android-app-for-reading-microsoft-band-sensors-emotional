/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import io.veedata.veemotion.R;

/**
 * A base view holder for cards use in a recycler view for the Emotional Fitness dashboard.
 * Instances of this class will have a top text view serving as the card's header.
 *
 * Created by Pablo Carbajal on 9/23/2016.
 */

abstract class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView mLabel;
    private String mColumnName;
    private Uri mContentUri;

    CardViewHolder(final View cardView) {
        super(cardView);

        cardView.setOnClickListener(this);

        mLabel = (TextView) cardView.findViewById(R.id.label);
    }

    public void bindData(final DashboardCardData data) {
        mLabel.setText(data.getLabel());
        mContentUri = data.getContentUri();
        mColumnName = data.getDataId();
    }

    String getColumnName() {
        return mColumnName;
    }

    Uri getContentUri() {
        return mContentUri;
    }

    @Override
    public void onClick(final View view) {
        // do nothing
    }
}
