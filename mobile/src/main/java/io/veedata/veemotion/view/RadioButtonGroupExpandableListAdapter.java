/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import io.veedata.veemotion.R;

/**
 * An adapter for {@link android.widget.ExpandableListView}s that groups radio buttons.
 * Created by Pablo Carbajal on 12/12/2016.
 */
public class RadioButtonGroupExpandableListAdapter extends BaseExpandableListAdapter {
    private final Context mContext;
    private final String[] mGroups;
    private final String[] mOptions;

    public RadioButtonGroupExpandableListAdapter(final Context context, final String[] groups, final String[] options) {
        mContext = context;
        mGroups = groups;
        mOptions = options;
    }

    @Override
    public int getGroupCount() {
        return mGroups.length;
    }

    /**
     * Always return a count of 1, regardless, as all the radio buttons are grouped under one radio group.
     * @param groupPosition
     * @return 1 child
     */
    @Override
    public int getChildrenCount(final int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(final int groupPosition) {
        return mGroups[groupPosition];
    }

    @Override
    public Object getChild(final int groupPosition, final int childPosition) {
        return mOptions[childPosition];
    }

    @Override
    public long getGroupId(final int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(final int groupPosition, final int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, final ViewGroup parent) {
        String listTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(android.R.layout.simple_expandable_list_item_1, null);
            ((TextView)convertView).setTextAppearance(R.style.AppTheme_QuestionGroupOption);
            convertView.setPadding(150, 20, 0, 20);
        }

        ((TextView)convertView).setText(listTitle);

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, final boolean isLastChild, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            final RadioGroup radioGroup = new RadioGroup(mContext);
            radioGroup.setPadding(55, 0, 0, 0);

            for (String radioButtonText : mOptions) {
                final RadioButton radioButton = new RadioButton(mContext);
                radioButton.setText(radioButtonText);

                radioButton.setTextAppearance(R.style.AppTheme_QuestionOption);
                radioGroup.addView(radioButton);
            }

            convertView = radioGroup;
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(final int groupPosition, final int childPosition) {
        return true;
    }
}
