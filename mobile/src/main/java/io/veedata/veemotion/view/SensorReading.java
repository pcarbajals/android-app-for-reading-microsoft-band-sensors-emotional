/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.net.Uri;

/**
 * Created by great_000 on 9/23/2016.
 */

public class SensorReading extends DashboardCardData {
    private final int mMipmapResource;

    public SensorReading(final String label, final int mipmapResource, final Uri contentUri, final String dataId) {
        super(label, contentUri, dataId);
        mMipmapResource = mipmapResource;
    }

    public int getMipmapResource() {
        return mMipmapResource;
    }

}
