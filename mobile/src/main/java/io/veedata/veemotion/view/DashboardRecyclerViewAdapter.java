/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import io.veedata.veemotion.R;

/**
 * Adapter for the DashboardRecycler view. It provides the cards showing the sensors data.
 * Created by Pablo A Carbajal on 9/23/2016.
 */
public class DashboardRecyclerViewAdapter extends android.support.v7.widget.RecyclerView.Adapter<CardViewHolder> {

    private final List<DashboardCardData> mDashboardCardDataList;

    public DashboardRecyclerViewAdapter(List<DashboardCardData> dashboardCardDataList) {
        mDashboardCardDataList = dashboardCardDataList;
    }

    @Override
    public CardViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(
                viewType, null);

        switch (viewType) {
            case R.layout.sensor_reading:
                return new HeartRateRealTimeViewHolder(layoutView);

            case R.layout.card_hr_chart:
                return new HeartRateChartViewHolder(layoutView);

            case R.layout.card_data_points_chart:
                return new DataPointsChartHolder(layoutView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final CardViewHolder holder, final int position) {
        DashboardCardData dashboardCardData = mDashboardCardDataList.get(position);
        holder.bindData(dashboardCardData);
    }

    @Override
    public int getItemViewType(final int position) {
        DashboardCardData dashboardCardData = mDashboardCardDataList.get(position);
        int viewType;
        if (dashboardCardData instanceof SensorReading) {
            viewType = R.layout.sensor_reading;

        } else if (dashboardCardData instanceof WeeklyHeartRateData) {
            viewType = R.layout.card_hr_chart;

        } else if (dashboardCardData instanceof DailyHeartRateCollectionData) {
            viewType = R.layout.card_data_points_chart;

        } else {
            viewType = super.getItemViewType(position);
        }

        return viewType;
    }

    @Override
    public int getItemCount() {
        return mDashboardCardDataList.size();
    }
}
