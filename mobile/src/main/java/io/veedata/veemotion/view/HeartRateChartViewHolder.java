/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.view.View;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import io.veedata.veemotion.database.sensor.HeartRateEventContract;
import io.veedata.veemotion.R;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;

/**
 * View for the card holding the chart with daily HR averages.
 *
 * Created by great_000 on 9/23/2016.
 */
class HeartRateChartViewHolder extends AbstractChartViewHolder {
    private static final String TAG = HeartRateChartViewHolder.class.getName();

    HeartRateChartViewHolder(final View cardView) {
        super(cardView);
    }

    @Override
    protected int getChartViewId() {
        return R.id.chart_hr_weekly;
    }

    @Override
    protected void drawChart() {
        DateTime now = DateTime.now();

        final ContentResolver contentResolver = itemView.getContext().getContentResolver();
        final Uri uri = HeartRateEventContract.CONTENT_URI;
        final String[] projection = new String[] {"avg(" + HeartRateEventContract.COLUMN_HEART_RATE + ")"};
        final String selection = HeartRateEventContract.COLUMN_TIME_STAMP + " BETWEEN ? AND ?";

        List<PointValue> dataPointValues = new ArrayList<PointValue>();
        List<AxisValue> axisValues = new ArrayList<AxisValue>();

        // start 7 days ago (zero-based count)
        for (int daysAgo = 6; daysAgo >= 0; daysAgo--) {
            final DateTime dayToQuery = now.minusDays(daysAgo);
            final String startOfDay = dayToQuery.withTime(0, 0, 0, 0).toString();
            final String endOfDay = dayToQuery.withTime(23, 59, 59, 999).toString();
            final String[] selectionArgs = new String[] { startOfDay, endOfDay };
            Cursor cursor = contentResolver.query(uri, projection, selection, selectionArgs, null);
            float heartRateAvg = 0.0f;
            if (cursor != null) {
                cursor.moveToFirst();
                heartRateAvg = cursor.getFloat(0);
                cursor.close();
            }

            int dataPointCount = dataPointValues.size();
            dataPointValues.add(new PointValue(dataPointCount, heartRateAvg).setLabel(Integer.toString(Math.round(heartRateAvg))));
            axisValues.add(new AxisValue(dataPointCount).setLabel(dayToQuery.dayOfWeek().getAsShortText()));
        }

        Line line = new Line(dataPointValues).setColor(Color.BLUE);
        line.setHasLabels(true);
        line.setStrokeWidth(2);
        line.setPointRadius(3);

        List<Line> lines = new ArrayList<>();
        lines.add(line);

        LineChartData chartData = new LineChartData();
        chartData.setLines(lines);

        Axis axisX = new Axis();
        axisX.setName("This week");

        axisX.setValues(axisValues);
        chartData.setAxisXBottom(axisX);

        Axis axisY = new Axis().setHasLines(true);
        axisY.setName("Heart Rate");
        chartData.setAxisYLeft(axisY);

        getChartView().setLineChartData(chartData);
        getChartView().setZoomEnabled(false);
    }
}
