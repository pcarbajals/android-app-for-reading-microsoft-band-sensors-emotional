/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.net.Uri;

/**
 * Created by great_000 on 9/23/2016.
 */
public abstract class DashboardCardData {
    protected final String mLabel;
    protected Uri mContentUri;
    protected String mDataId;

    public DashboardCardData(final String label, final Uri contentUri, final String dataId) {
        mLabel = label;
        mContentUri = contentUri;
        mDataId = dataId;
    }

    public String getLabel() {
        return mLabel;
    }

    public Uri getContentUri() {
        return mContentUri;
    }

    public String getDataId() {
        return mDataId;
    }

}
