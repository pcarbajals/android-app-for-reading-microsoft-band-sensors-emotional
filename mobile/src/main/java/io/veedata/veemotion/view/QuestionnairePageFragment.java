/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.veedata.veemotion.R;

import static io.veedata.veemotion.QuestionnaireActivity.ARG_QUESTIONNAIRE_ID;

/**
 * A placeholder fragment containing a the view for the questionnaire's responses.
 */
public class QuestionnairePageFragment extends Fragment {

    /**
     * The bundle argument representing the question number that this page fragment displays.
     */
    public static final String ARG_QUESTION_ID = "question_number";

    /**
     * Indicates that the question to display should use radio buttons for its responses.
     */
    private static final String QUESTION_TYPE_RADIOBUTTON = "radiobutton";

    /**
     * Indicates that the question to display should use a spinner for its responses.
     */
    private static final String QUESTION_TYPE_SPINNER = "spinner";

    /**
     * Indicates that the question to display should use check boxes for its responses.
     */
    private static final String QUESTION_TYPE_CHECKBOX = "checkbox";

    /**
     * Indicates that the question to display has groups of radio buttons for its responses.
     */
    private static final String QUESTION_TYPE_RADIOBUTTON_GROUP = "radiobuttongroup";

    public QuestionnairePageFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given question number.
     */
    public static QuestionnairePageFragment newInstance(final int questionnaireId, int questionNumber) {
        QuestionnairePageFragment fragment = new QuestionnairePageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_QUESTIONNAIRE_ID, questionnaireId);
        args.putInt(ARG_QUESTION_ID, questionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_questionnaire, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.label_question);

        final int questionnaireId = getArguments().getInt(ARG_QUESTIONNAIRE_ID);
        final int questionId = getArguments().getInt(ARG_QUESTION_ID);

        // Set the text for the question
        final String title = getStringResource("title", questionnaireId, questionId);
        textView.setText(title);

        final LinearLayout questionnaireLayout = (LinearLayout) rootView.findViewById(R.id.layout_questionnaire);
        final QuestionnaireResponsesFactory factory;

        // Instantiate a factory for creating the questionnaire's responses based on the presentation type
        final String presentationType = getStringResource("type", questionnaireId, questionId);
        switch (presentationType) {
            case QUESTION_TYPE_SPINNER:
                final String[] responses = getResponsesAsArray(questionnaireId, questionId);
                factory = QuestionnaireResponsesFactory.createSpinnerFactory(responses, questionnaireLayout);
                break;

            case QUESTION_TYPE_CHECKBOX: {
                final String[] options = getResponsesAsArray(questionnaireId, questionId);
                factory = QuestionnaireResponsesFactory.createCheckboxFactory(options, questionnaireLayout);
                break;
            }
            case QUESTION_TYPE_RADIOBUTTON_GROUP: {
                final String[] groups = getArray(questionnaireId, questionId, "groups");
                final String[] options = getResponsesAsArray(questionnaireId, questionId);
                factory = QuestionnaireResponsesFactory.createRadioButtonGroupsFactory(groups, options, questionnaireLayout);
                break;
            }
            default: {
                final String[] options = getResponsesAsArray(questionnaireId, questionId);
                factory = QuestionnaireResponsesFactory.createRadioButtonsFactory(options, questionnaireLayout);
                break;
            }
        }

        factory.createView();

        return rootView;
    }

    @NonNull
    private String[] getResponsesAsArray(final int questionnaireId, final int questionId) {
        final String options = "options";
        return getArray(questionnaireId, questionId, options);
    }

    @NonNull
    private String[] getArray(final int questionnaireId, final int questionId, final String resourceSuffix) {
        final int optionsId = getResources().getIdentifier("question_" + questionnaireId + "_" + questionId + "_" + resourceSuffix, "array", getActivity().getPackageName());
        return getResources().getStringArray(optionsId);
    }

    /**
     * Returns the questionnaire string resource specified in the suffix (e.g. "title" or "type").
     * @param stringSuffix either "title" for the question's title or "type" for radiobutton, spinner, checkbox or matrix
     * @param questionnaireId the questionnarie's id to look for
     * @param questionId the question's id to look for
     * @return
     */
    @NonNull
    private String getStringResource(final String stringSuffix, final int questionnaireId, final int questionId) {
        final int titleId = getResources().getIdentifier("question_" + questionnaireId + "_" + questionId + "_" + stringSuffix, "string", getActivity().getPackageName());

        if (titleId == 0) {
            return "";
        }

        return getResources().getString(titleId);
    }

}
