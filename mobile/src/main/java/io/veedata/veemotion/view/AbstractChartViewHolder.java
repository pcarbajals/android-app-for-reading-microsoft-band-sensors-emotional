/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.os.Handler;
import android.view.View;

import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * Created by great_000 on 11/22/2016.
 */
abstract class AbstractChartViewHolder extends CardViewHolder {
    private final LineChartView mChartView;

    /**
     * Handler for executing the command that draws the chart
     */
    Handler mDrawChartHandler = new Handler();

    /**
     * Command called by the handler to update the chart
     */
    Runnable mDrawChartCommand = new Runnable() {

        @Override
        public void run() {
            drawChart();
        }
    };

    AbstractChartViewHolder(final View cardView) {
        super(cardView);
        mChartView = (LineChartView) cardView.findViewById(getChartViewId());
    }

    protected abstract int getChartViewId();

    @Override
    public void bindData(final DashboardCardData data) {
        super.bindData(data);

        updateChart();
    }

    protected void updateChart() {
        LineChartData emptyChartData = new LineChartData();
        getChartView().setLineChartData(emptyChartData);

        mDrawChartHandler.postDelayed(mDrawChartCommand, 0);
    }

    protected abstract void drawChart();

    LineChartView getChartView() {
        return mChartView;
    }
}
