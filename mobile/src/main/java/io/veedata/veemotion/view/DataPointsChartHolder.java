/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.veedata.veemotion.database.sensor.BandDataContract;
import io.veedata.veemotion.database.sensor.ConnectionStateEventContract;
import io.veedata.veemotion.database.sensor.ContactStateEventContract;
import io.veedata.veemotion.database.sensor.HeartRateEventContract;
import io.veedata.veemotion.R;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;

/**
 * View for the card holding the chart with data points collected over a specific period of time.
 * <p>
 * Created by Pablo A. Carbajal on 11/18/2016.
 */
class DataPointsChartHolder extends AbstractChartViewHolder implements AdapterView.OnItemSelectedListener {
    private static final String TAG = DataPointsChartHolder.class.getName();
    private final Spinner mPeriodValueSpinner;
    private final String[] mPeriodValues;

    DataPointsChartHolder(final View cardView) {
        super(cardView);

        // set up period values for the spinner
        mPeriodValueSpinner = (Spinner) cardView.findViewById(R.id.spinner_period_value);
        mPeriodValueSpinner.setOnItemSelectedListener(this);

        mPeriodValues = cardView.getResources().getStringArray(R.array.data_points_chart_periods);
    }

    @Override
    protected void drawChart() {
        int timeFrameInHours = Integer.parseInt(mPeriodValueSpinner.getSelectedItem().toString());
        final DateTime now = DateTime.now(DateTimeZone.UTC);
        final DateTime startOfPeriod = now.minusHours(timeFrameInHours);

        Map<Integer, Integer> hrPointsPerMinute = generateHrPointsPerMinute(now, startOfPeriod);
        Map<Integer, String> contactStatePerMinute = generateContactPointsPerMinute(now, startOfPeriod);
        Map<Integer, String> connectionStatePerMinute = generateConnectionStatePerMinute(now, startOfPeriod);

        List<PointValue> hrLineValues = new ArrayList<>();
        List<PointValue> stateLineValues = new ArrayList<>();

        List<AxisValue> axisHrValues = new ArrayList<>();

        int totalMinutesInPeriod = Minutes.minutesBetween(startOfPeriod, now).getMinutes();
        for (int currentMinute = 0; currentMinute <= totalMinutesInPeriod; currentMinute++) {

            /*
             * Get the HR data point for the current minute and add it to the line,
             * if the point doesn't exist, then add a zero point to show a drop in the line
             */
            if (hrPointsPerMinute.containsKey(currentMinute)) {
                final Integer hrPoints = hrPointsPerMinute.get(currentMinute);
                hrLineValues.add(new PointValue(currentMinute, hrPoints));

            } else {
                final Integer hrPoints = 0;
                hrLineValues.add(new PointValue(currentMinute, hrPoints));
            }

            /*
              * Get the contact state for the current minute. By default, the point value is zero,
              * however if the state is "WORN" then set the point value to 30. Given that the
              * HR point values range from 0 to around 60, choosing a value of 30 for "WORN" will
              * make the contact state point values display about midway of the y-axis.
              */
            if (contactStatePerMinute.containsKey(currentMinute)) {
                final String contactState = contactStatePerMinute.get(currentMinute);
                float value = 120f;
                if (contactState.equals("WORN")) {
                    value = 160f;
                }

                stateLineValues.add(new PointValue(currentMinute, value));
            }

            if (connectionStatePerMinute.containsKey(currentMinute)) {
                final String connectionState = connectionStatePerMinute.get(currentMinute);
                float value = 0f;
                if (connectionState.equals("BOUND")) {
                    value = 40f;
                } else if (connectionState.equals("CONNECTED")) {
                    value = 80f;
                }

                stateLineValues.add(new PointValue(currentMinute, value));
            }

            /*
             * Add x-axis labels every hour as "12 am", "1 pm", etc.
             */
            if (currentMinute % 60 == 0) {
                DateTimeFormatter fmt = DateTimeFormat.forPattern("K a");
                axisHrValues.add(new AxisValue(currentMinute).setLabel(startOfPeriod.plusMinutes(currentMinute).toDateTime(DateTimeZone.getDefault()).toString(fmt)));
            }
        }

        List<Line> lines = new ArrayList<>();

        final Line hrLine = new Line(hrLineValues).setColor(Color.BLUE);
        hrLine.setHasPoints(false);
        hrLine.setStrokeWidth(1);
        lines.add(hrLine);

        final Line contactLine = new Line(stateLineValues).setColor(Color.DKGRAY);
        contactLine.setHasLines(false);
        contactLine.setHasPoints(true);
        contactLine.setPointRadius(3);
        lines.add(contactLine);

        LineChartData chartData = new LineChartData();
        chartData.setLines(lines);

        final Axis axisX = new Axis();
        axisX.setValues(axisHrValues);
        chartData.setAxisXBottom(axisX);

/*
        final Axis hrAxisY = new Axis().setHasLines(false);
        hrAxisY.setName("HR Data Points");
        chartData.setAxisYLeft(hrAxisY);
*/

        final List<AxisValue> stateAxisYValues = new ArrayList<>();
        stateAxisYValues.add(new AxisValue(0).setLabel("Unknown"));
        stateAxisYValues.add(new AxisValue(40f).setLabel("Bound"));
        stateAxisYValues.add(new AxisValue(80f).setLabel("Connected"));
        stateAxisYValues.add(new AxisValue(120f).setLabel("Not Worn"));
        stateAxisYValues.add(new AxisValue(160f).setLabel("Worn"));


        final Axis stateAxisY = new Axis().setHasLines(true);
        stateAxisY.setName(" ");
        stateAxisY.setValues(stateAxisYValues);
        stateAxisY.setHasTiltedLabels(true);
        chartData.setAxisYLeft(stateAxisY);

        getChartView().setLineChartData(chartData);
        getChartView().setZoomEnabled(false);
    }

    private Map<Integer, String> generateConnectionStatePerMinute(final DateTime now, final DateTime startOfPeriod) {
        final ContentResolver contentResolver = itemView.getContext().getContentResolver();
        final Uri uri = ConnectionStateEventContract.CONTENT_URI;
        final String[] projection = new String[] { BandDataContract.COLUMN_TIME_STAMP, ConnectionStateEventContract.COLUMN_NAME_STATE };
        final String selection = "(" + BandDataContract.COLUMN_TIME_STAMP + " BETWEEN ? AND ?)";
        final String[] selectionArgs = new String[] { startOfPeriod.toString(), now.toString() };

        Cursor cursor = contentResolver.query(uri, projection, selection, selectionArgs, null);

        Map<Integer, String> dataPointsPerMinute = new HashMap<>();

        if (cursor != null) {
            cursor.moveToFirst();

            while (cursor.moveToNext()) {
                DateTime timeStamp = new DateTime(cursor.getString(0));
                int minutes = Minutes.minutesBetween(startOfPeriod, timeStamp).getMinutes();
                dataPointsPerMinute.put(minutes, cursor.getString(1));
            }

            cursor.close();
        }
        return dataPointsPerMinute;
    }

    private Map<Integer, String> generateContactPointsPerMinute(final DateTime now, final DateTime startOfPeriod) {
        final ContentResolver contentResolver = itemView.getContext().getContentResolver();
        final Uri uri = ContactStateEventContract.CONTENT_URI;
        final String[] projection = new String[] { BandDataContract.COLUMN_TIME_STAMP, ContactStateEventContract.COLUMN_NAME_STATE };
        final String selection = "(" + BandDataContract.COLUMN_TIME_STAMP + " BETWEEN ? AND ?)";
        final String[] selectionArgs = new String[] { startOfPeriod.toString(), now.toString() };

        Cursor cursor = contentResolver.query(uri, projection, selection, selectionArgs, null);

        Map<Integer, String> dataPointsPerMinute = new HashMap<>();

        if (cursor != null) {
            cursor.moveToFirst();

            while (cursor.moveToNext()) {
                DateTime timeStamp = new DateTime(cursor.getString(0));
                int minutes = Minutes.minutesBetween(startOfPeriod, timeStamp).getMinutes();
                dataPointsPerMinute.put(minutes, cursor.getString(1));
            }

            cursor.close();
        }
        return dataPointsPerMinute;
    }

    @NonNull
    private Map<Integer, Integer> generateHrPointsPerMinute(final DateTime now, final DateTime startOfPeriod) {
        final ContentResolver contentResolver = itemView.getContext().getContentResolver();
        final Uri uri = HeartRateEventContract.CONTENT_URI;
        final String[] projection = new String[] { BandDataContract.COLUMN_TIME_STAMP };
        final String selection = "(" + BandDataContract.COLUMN_TIME_STAMP + " BETWEEN ? AND ?) AND (" + HeartRateEventContract.COLUMN_QUALITY + " = ?)";
        final String[] selectionArgs = new String[] { startOfPeriod.toString(), now.toString(), HeartRateEventContract.COLUMN_QUALITY_VALUE_LOCKED };

        Cursor cursor = contentResolver.query(uri, projection, selection, selectionArgs, null);

        Map<Integer, Integer> dataPointsPerMinute = new HashMap<Integer, Integer>();

        if (cursor != null) {
            cursor.moveToFirst();

            while (cursor.moveToNext()) {
                DateTime timeStamp = new DateTime(cursor.getString(0));
                int minutes = Minutes.minutesBetween(startOfPeriod, timeStamp).getMinutes();
                if (dataPointsPerMinute.containsKey(minutes)) {
                    int count = dataPointsPerMinute.get(minutes);
                    count++;
                    dataPointsPerMinute.put(minutes, count);
                } else {
                    dataPointsPerMinute.put(minutes, 1);
                }
            }

            cursor.close();
        }
        return dataPointsPerMinute;
    }

    @Override
    protected int getChartViewId() {
        return R.id.chart_data_points;
    }

    @Override
    public void onItemSelected(final AdapterView<?> adapterView, final View view, final int i, final long l) {
        updateChart();
    }

    @Override
    public void onNothingSelected(final AdapterView<?> adapterView) {
        // nothing to do here
    }
}
