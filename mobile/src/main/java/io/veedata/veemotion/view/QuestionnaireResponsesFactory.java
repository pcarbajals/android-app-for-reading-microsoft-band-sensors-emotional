/*
 * Licensed under the Non-Profit Open Software License version 3.0
 *
 * Copyright (c) 2017. Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package io.veedata.veemotion.view;

import android.content.Context;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import io.veedata.veemotion.R;

/**
 * Use this class for creating the questionnaire's mOptions and adding them as views to a
 * {@link ViewGroup}. The mOptions will be laid out as radio buttons, however tf the number of
 * mOptions is greater than 7 then the mOptions will be laid out as a spinner. This last variant
 * assumes the first option is a generic string such as "Choose an option" and the last option
 * is a string indicating an unspecified option such as "Other...".
 * <p>
 * Created by Pablo Carbajal on 12/5/2016.
 */
abstract class QuestionnaireResponsesFactory {
    private static final int MAX_OTHER_TEXT_LENGTH = 30;
    private String[] mOptions;
    private final ViewGroup mViewGroup;

    private QuestionnaireResponsesFactory(final String[] options, final ViewGroup viewGroup) {
        mOptions = options;
        mViewGroup = viewGroup;
    }

    private QuestionnaireResponsesFactory(final ViewGroup container) {
        mViewGroup = container;
    }

    protected abstract void createView();

    String[] getOptions() {
        return mOptions;
    }

    protected ViewGroup getViewGroup() {
        return mViewGroup;
    }

    public static QuestionnaireResponsesFactory createRadioButtonsFactory(final String[] options, final ViewGroup container) {
        return new QuestionnaireRadioButtonsFactory(options, container);
    }

    public static QuestionnaireResponsesFactory createSpinnerFactory(final String[] options, final ViewGroup container) {
        return new QuestionnaireSpinnerWithTextInputFactory(options, container);
    }

    public static QuestionnaireResponsesFactory createCheckboxFactory(final String[] options, final ViewGroup container) {
        return new QuestionnaireCheckboxFactory(options, container);
    }

    public static QuestionnaireResponsesFactory createRadioButtonGroupsFactory(final String[] groups, final String[] options, final ViewGroup container) {
        return new QuestionnaireRadioButtonGroupsFactory(groups, options, container);
    }

    /**
     * This class builds the questionnaire response mOptions as a group of radio buttons.
     * <p>
     * Created by Pablo Carbajal on 12/5/2016.
     */
    private static class QuestionnaireRadioButtonsFactory extends QuestionnaireResponsesFactory {
        QuestionnaireRadioButtonsFactory(final String[] options, final ViewGroup container) {
            super(options, container);
        }

        @Override
        public void createView() {
            final RadioGroup radioGroup = new RadioGroup(getViewGroup().getContext());
            radioGroup.setPadding(55, 0, 0, 0);
            getViewGroup().addView(radioGroup);

            for (final String option : getOptions()) {
                final RadioButton radioButton = new RadioButton(getViewGroup().getContext());
                radioButton.setText(option);

                radioButton.setTextAppearance(R.style.AppTheme_QuestionOption);
                radioGroup.addView(radioButton);
            }
        }
    }

    /**
     * This class builds the questionnaire response mOptions as a spinner with a text input view for a
     * "Other..." option. Note that this class assumes the first option is a generic string such as
     * "Choose an option" and the last option is a string indicating an unspecified option such as
     * "Other...".
     * <p>
     * Created by Pablo Carbajal on 12/5/2016.
     */
    private static class QuestionnaireSpinnerWithTextInputFactory extends QuestionnaireResponsesFactory implements AdapterView.OnItemSelectedListener {

        private EditText mOtherText;

        QuestionnaireSpinnerWithTextInputFactory(final String[] options, final ViewGroup container) {
            super(options, container);
        }

        @Override
        public void createView() {
            final Spinner optionsSpinner = new Spinner(getViewGroup().getContext());
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getViewGroup().getContext(), R.layout.large_text_spinner_layout, getOptions());
            arrayAdapter.setDropDownViewResource(R.layout.large_text_spinner_layout);
            optionsSpinner.setAdapter(arrayAdapter);
            optionsSpinner.setOnItemSelectedListener(this);

            /* Set the text field for the "Other..." option in the spinner. Initially it is invisible,
             * However when the user selects "Other..." from the spinner, this field becomes visible.
             */
            mOtherText = new EditText(getViewGroup().getContext());
            mOtherText.setSingleLine(true);
            mOtherText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_OTHER_TEXT_LENGTH)});
            mOtherText.setHint("Type in the activity...");
            mOtherText.setVisibility(View.GONE);

            // add the spinner and the text field to the layout
            getViewGroup().addView(optionsSpinner);
            getViewGroup().addView(mOtherText);
        }

        /**
         * <p>Callback method to be invoked when an option in the spinner has been
         * selected. This callback is invoked only when the newly selected
         * position is different from the previously selected position or if
         * there was no selected item.</p>
         * <p/>
         * If the last option (i.e. "Other...") is selected, then the Other text view will appear.
         * Otherwise, when any of the other mOptions is selected, the Other text view will disappear.
         *
         * @param parent   The AdapterView where the selection happened
         * @param view     The view within the AdapterView that was clicked
         * @param position The position of the view in the adapter
         * @param id       The row id of the item that is selected
         */
        @Override
        public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
            CharSequence selectedItem = (CharSequence) parent.getSelectedItem();
            final String lastOption = getOptions()[getOptions().length - 1];

            // enable the text view if the "Other..." item is selected
            if (selectedItem.toString().equals(lastOption)) {
                mOtherText.setVisibility(View.VISIBLE);
            } else {
                mOtherText.setVisibility(View.GONE);
            }
        }

        @Override
        public void onNothingSelected(final AdapterView<?> adapterView) {
            // do nothing
        }
    }

    /**
     * This class builds the questionnaire's responses as checkboxes with a text input view for an
     * "Other..." option. Note that if this factory finds the last option is "Other..." then it will
     * create a text field for the user to type in another option.
     * <p>
     * Created by Pablo Carbajal on 12/7/2016.
     */
    private static class QuestionnaireCheckboxFactory extends QuestionnaireResponsesFactory {
        public QuestionnaireCheckboxFactory(final String[] options, final ViewGroup container) {
            super(options, container);
        }

        @Override
        protected void createView() {
            for (final String option : getOptions()) {
                final CheckBox checkBox = new CheckBox(getViewGroup().getContext());
                checkBox.setTextAppearance(R.style.AppTheme_QuestionOption);

                if (option.equals("...")) {
                    final LinearLayout editableCheckBoxContainer = new LinearLayout(getViewGroup().getContext());
                    editableCheckBoxContainer.setOrientation(LinearLayout.HORIZONTAL);

                    final EditText editText = new EditText(getViewGroup().getContext());
                    editText.setSingleLine(true);
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_OTHER_TEXT_LENGTH)});
                    editText.setMinimumWidth(MAX_OTHER_TEXT_LENGTH);
                    editText.setHint("Other...");

                    editableCheckBoxContainer.addView(checkBox);
                    editableCheckBoxContainer.addView(editText);
                    getViewGroup().addView(editableCheckBoxContainer);

                } else {
                    checkBox.setText(option);
                    getViewGroup().addView(checkBox);

                }
            }
        }
    }

    private static class QuestionnaireRadioButtonGroupsFactory extends QuestionnaireResponsesFactory {
        private String[] mGroups;
        private String[] mOptions;

        QuestionnaireRadioButtonGroupsFactory(final String[] groups, final String[] options, final ViewGroup container) {
            super(container);
            mGroups = groups;
            mOptions = options;
        }

        @Override
        protected void createView() {
            final Context context = getViewGroup().getContext();

            final ExpandableListView expandableListView = new ExpandableListView(context);
            expandableListView.setAdapter(new RadioButtonGroupExpandableListAdapter(context, mGroups, mOptions));
            getViewGroup().addView(expandableListView);
        }

    }
}
